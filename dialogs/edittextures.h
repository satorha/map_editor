#ifndef EDITTEXTURES_H
#define EDITTEXTURES_H

#include <QDialog>

namespace Ui {
class EditTextures;
}

class EditTextures : public QDialog
{
    Q_OBJECT

public:
    explicit EditTextures(QWidget *parent = 0);
    ~EditTextures();

private slots:
    void on_pushButton_2_clicked();
    void seccaoSelecionada(const QString& sec);
    void itemSelecionado(const QString& sec, const QString& nome);

    void on_pushButton_9_clicked();

private:
    Ui::EditTextures *ui;

    void habilitarControles(bool estado);
};

#endif // EDITTEXTURES_H
