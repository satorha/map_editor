#include "picturebrowser.h"
#include <algorithm>
#include <QDebug>
#include <QMouseEvent>

const int altura_seccao = 200;
const int lado_thumbnail = altura_seccao * 0.9;

PictureBrowser::PictureBrowser(QWidget *parent) :
    QFrame(parent)
{
    m_ultimo_selecionado = nullptr;
}

void PictureBrowser::addSection(QString nome)
{
    if(getSection(nome) != nullptr)
        return; //já existe uma seção com este nome

    m_seccoes.push_back(secao());
    m_seccoes.back().titulo = nome;

    this->update();
}

void PictureBrowser::deleteSection(QString nome)
{
    for(std::list<secao>::iterator it=m_seccoes.begin(); it != m_seccoes.end(); it++)
    {
        if((*it).titulo == nome)
        {
            m_seccoes.erase(it);
            this->update();
            return;
        }
    }
}

bool PictureBrowser::addItem(const QImage &thumb, QString sec, QString nome)
{
    secao* s = getSection(sec);
    if(s == nullptr)
    {
        addSection(sec);
        s = getSection(sec);
    }

    //checa se ja tem um objeto com este nome
    for(item& i : s->itens)
    {
        if(i.name == nome)
        {
            qDebug() << "PictureBrowser::addItem(): Já existe um item com este nome.";
            return false;
        }
    }

    //adiciona o objeto
    s->itens.push_back(item());
    item& adicionado = s->itens.back();
    adicionado.name = nome;
    adicionado.thumbnail = thumb;
    adicionado.seccao = s;
    this->update();

    return true;
}

void PictureBrowser::deleteItem(const QString& sec, const QString& str)
{
    secao* s = getSection(sec);
    if(s == nullptr)
    {
        qDebug() << "deleteItem(): seção não encontrada.";
        return;
    }

    for(std::list<item>::iterator it = s->itens.begin(); it != s->itens.end(); it++)
    {
        if((*it).name == str)
        {
            if(m_ultimo_selecionado == &(*it))
            {
                m_ultimo_selecionado = nullptr;
            }
            s->itens.erase(it);
            this->update();
            return;
        }
    }
}

PictureBrowser::secao* PictureBrowser::getSection(QString nome)
{
    for(secao& s : m_seccoes)
    {
        if(nome == s.titulo)
        {
            return &s;
        }
    }
    return nullptr;
}

void PictureBrowser::desenharSeccao(PictureBrowser::secao& s, QPainter& pintor, int y_atual)
{
    int texto_s = altura_seccao-lado_thumbnail;
    pintor.drawText(QRect(0, y_atual+texto_s*0.15, this->width(), texto_s*0.85), s.titulo);

    int x_atual = altura_seccao*0.05;
    for(item& i : s.itens)
    {
        i.area = QRect(x_atual, y_atual+(altura_seccao-lado_thumbnail), lado_thumbnail*0.95, lado_thumbnail*0.95);
        pintor.drawImage(i.area, i.thumbnail.scaled(QSize(i.area.width(), i.area.height()), Qt::KeepAspectRatio));

        if(m_ultimo_selecionado == &i)
        {
            pintor.fillRect(i.area, QBrush(QColor(0, 0, 255, 128)));
        }
        x_atual += lado_thumbnail;
    }
}

void PictureBrowser::paintEvent(QPaintEvent *e)
{
    int widget_h = m_seccoes.size()*altura_seccao*1.1;
    int widget_w = 0;
    for(secao& atual : m_seccoes)
    {
        int val = atual.itens.size()*lado_thumbnail;
        widget_w = (widget_w > val)? widget_w : val;
    }
    widget_w += altura_seccao*0.05;

    setMinimumSize(widget_w, widget_h);


    int y_atual = 0;
    QPainter pintor(this);
    pintor.fillRect(QRect(0, 0, width(), height()), QColor(128, 128, 128));
    for(secao& atual : m_seccoes)
    {
        QColor cor(255, 255, 255);
        pintor.fillRect(QRect(0, y_atual, width(), altura_seccao), cor);
        atual.area = QRect(0, y_atual, width(), altura_seccao);

        desenharSeccao(atual, pintor, y_atual);
        y_atual += altura_seccao*1.1;
    }
}


void PictureBrowser::mouseReleaseEvent(QMouseEvent * event)
{
    bool section_c = false;
    for(secao& atual : m_seccoes)
    {
        if(atual.area.contains(event->x(), event->y()))
        {
            section_c = true;
            emit sectionClicked(atual.titulo);
        }

        for(item& i : atual.itens)
        {
            if(i.area.contains(event->x(), event->y()))
            {
                emit itemClicked(atual.titulo, i.name);
                setSelecion(atual.titulo, i.name);
                return;
            }
        }
    }

    //nao clicou em nenhum
    m_ultimo_selecionado = nullptr;
    this->update();

    if(!section_c)
        emit sectionClicked(QString());

}


void PictureBrowser::moveLeft()
{
    if(m_ultimo_selecionado == nullptr)
        return;

    secao* sec = m_ultimo_selecionado->seccao;

    std::list<item>::iterator it = sec->itens.begin();
    for(; it != sec->itens.end(); it++)
    {
        if((*it).name == m_ultimo_selecionado->name)
        {
            //se for no primeiro, a operação é impossível
            if(it == sec->itens.begin())
                return;
            std::list<item>::iterator anterior = std::prev(it);
            std::swap(*it, *anterior);
            setSelecion(sec->titulo, (*anterior).name);
            break;
        }

    }
}

void PictureBrowser::moveRight()
{
    if(m_ultimo_selecionado == nullptr)
        return;

    secao* sec = m_ultimo_selecionado->seccao;

    std::list<item>::iterator it = sec->itens.begin();
    for(; it != sec->itens.end(); it++)
    {
        if((*it).name == m_ultimo_selecionado->name)
        {
            //se for no ultimo, a operação é impossível
            if(it == std::prev(sec->itens.end()))
                return;
            std::list<item>::iterator anterior = std::next(it);
            std::swap(*it, *anterior);
            setSelecion(sec->titulo, (*anterior).name);
            break;
        }

    }
}

void PictureBrowser::setSelecion(QString sec, QString nome)
{
    secao* s = getSection(sec);
    if(s == nullptr)
        return;

    for(item& i : s->itens)
    {
        if(i.name == nome)
        {
            m_ultimo_selecionado = &i;
            this->update();
            return;
        }
    }
}


const PictureBrowser::item* PictureBrowser::getItem(QString sec, QString nome)
{
    secao* s = getSection(sec);
    if(s == nullptr)
        return nullptr;

    for(item& i : s->itens)
    {
        if(i.name == nome)
        {
            return &i;
        }
    }
    return nullptr;
}
