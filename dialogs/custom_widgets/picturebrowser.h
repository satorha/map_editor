#ifndef PICTUREBROWSER_H
#define PICTUREBROWSER_H

#include <QFrame>
#include <QImage>
#include <list>
#include <QPainter>
#include <algorithm>

class PictureBrowser : public QFrame
{
    Q_OBJECT
public:
    struct secao;

    struct item
    {
        QString name;
        QImage thumbnail;
        QRect area;
        secao* seccao;
    };
    struct secao
    {
        QString titulo;
        std::list<item> itens;
        QRect area;
    };

    explicit PictureBrowser(QWidget *parent = 0);

    void addSection(QString nome);
    void deleteSection(QString nome);

    bool addItem(const QImage& thumb, QString secao, QString nome);
    void deleteItem(const QString& secao, const QString& str);

    void clear();


    void mouseReleaseEvent(QMouseEvent * event);

    void setSelecion(QString secao, QString nome); //seleciona o objeto especificado

    const item* getItem(QString sec, QString nome);
signals:
    void itemClicked(const QString& seccao, const QString& nome);
    void sectionClicked(const QString& seccao);


public slots:
    void moveRight(); //move o objeto atualmente selecionado pra direita
    void moveLeft(); //move o objeto atualmente selecionado pra esquerda

protected:
    void paintEvent(QPaintEvent *e);

private:
    item* m_ultimo_selecionado;


    int m_next_id;

    secao* getSection(QString nome);

    void desenharSeccao(secao& s, QPainter& pintor, int y);


    std::list<secao> m_seccoes;

};

#endif // PICTUREBROWSER_H
