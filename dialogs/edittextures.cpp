#include "edittextures.h"
#include "ui_edittextures.h"
#include <QInputDialog>
#include <QDebug>

EditTextures::EditTextures(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EditTextures)
{
    ui->setupUi(this);

    connect(ui->pushButton_7, SIGNAL(clicked()), ui->frame, SLOT(moveLeft()));

    connect(ui->pushButton_8, SIGNAL(clicked()), ui->frame, SLOT(moveRight()));
    connect(ui->frame, SIGNAL(sectionClicked(QString)), this, SLOT(seccaoSelecionada(QString)));
    connect(ui->frame, SIGNAL(itemClicked(QString,QString)), this, SLOT(itemSelecionado(QString,QString)));

    QImage teste("teste.jpg");
    QImage teste2("4307.png");

    ui->frame->addItem(teste, "Imagens de teste1:", "imagem 1");
    ui->frame->addItem(teste, "Imagens de teste2:", "imagem 2");
    ui->frame->addItem(teste2, "Imagens de teste3:", "imagem 1");
    ui->frame->addItem(teste2, "Imagens de teste2:", "imagem 3");

    habilitarControles(false);


}

EditTextures::~EditTextures()
{
    delete ui;
}

//botão de adicionar novo estado
void EditTextures::on_pushButton_2_clicked()
{
    QString nome = QInputDialog::getText(this, "Novo estado", "Nome pra animação:");
    if(nome.isEmpty())
        return;
    ui->frame->addSection(nome);

}

void EditTextures::habilitarControles(bool estado)
{
    qDebug() << "estado: " << estado;
    ui->pushButton_10->setEnabled(estado);
    ui->lineEdit->setEnabled(estado);
    ui->pushButton->setEnabled(estado);
    ui->spinBox->setEnabled(estado);
    ui->pushButton_4->setEnabled(estado);
    ui->pushButton_5->setEnabled(estado);
    ui->pushButton_6->setEnabled(estado);
    ui->lineEdit_2->setEnabled(estado);
    ui->pushButton_9->setEnabled(estado);
}

void EditTextures::seccaoSelecionada(const QString &sec)
{
    ui->lineEdit_2->setText(QString());
    ui->label->setPixmap(QPixmap());
    if(sec.isEmpty())
    { //limpa a ui
        habilitarControles(false);
    }
    else
    {
        habilitarControles(true);

        //preenche a UI
        ui->lineEdit_2->setText(sec);
    }
}

void EditTextures::itemSelecionado(const QString &sec, const QString &nome)
{
    if(nome.isEmpty() || sec.isEmpty())
    {
        return;
    }

    const PictureBrowser::item* item = ui->frame->getItem(sec, nome);
    if(item == nullptr)
    {
        qDebug() << "ponteiro a item é nulo mas a string não. (?)";
        return;
    }

    QImage imagem = item->thumbnail.scaled(std::min(200, item->thumbnail.width()), std::min(200, item->thumbnail.height()), Qt::KeepAspectRatio);
    ui->label->setPixmap(QPixmap::fromImage(imagem));


}

void EditTextures::on_pushButton_9_clicked() // add frame
{


}
