#ifndef GLWIDGET_H
#define GLWIDGET_H

 #include <QGLWidget>
#include "Scene/Scene.h"
#include <memory>


#include "controlador_janela_base.h"
class GLWidget : public QGLWidget
{
    Q_OBJECT        // must include this if you use Qt signals/slots

public:
    GLWidget(QWidget *parent);
    ~GLWidget();

    Scene* _ena_ativa;
protected:
    void mousePressEvent( QMouseEvent * event);
    void mouseReleaseEvent( QMouseEvent * event);

    void initializeGL();

    void resizeGL(int w, int h);

    void paintGL();
private:
    std::unique_ptr<controlador_janela_base> modo_atual;
    void setMode(controlador_janela_base* n);

};
#endif // GLWIDGET_H
