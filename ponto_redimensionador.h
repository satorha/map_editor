#ifndef PONTO_REDIMENSIONADOR_H
#define PONTO_REDIMENSIONADOR_H

#include "engine/Scene/Controller/Interface.h"
#include "engine/Scene/Sprite.h"

#include "obj_controller.h"
class ponto_redimensionador : public Interface
{
public:
    ponto_redimensionador();

    virtual const char* getName();

    void setParent(obj_controller* entidade)
    {
        m_parent = entidade;
    }

    void setPos(int p)
    {
        m_pos = p;
    }

    virtual void eventMouseButtonDown();

private:
    obj_controller* m_parent;
    int m_pos;
};

Interface* alocar_ponto_redimensionador();

#endif // PONTO_REDIMENSIONADOR_H
