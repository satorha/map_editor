#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "engine/Scene/Entity.h"
#include <QTableWidget>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void setEdit(Entity* obj);
private slots:
    void on_actionAdicionar_objeto_triggered();

    void on_tableWidget_itemDoubleClicked(QTableWidgetItem *item);

    void on_tableWidget_cellDoubleClicked(int row, int column);

    void on_actionInserir_sprite_triggered();

    void on_actionAdicionar_texto_triggered();

    void on_actionMover_Objeto_triggered();

private:
    Entity* editando;

    void addTableData(QString nome1, QString nome2);
    void clearTable();
    void prompt_data_for_entity(QString data);

    Ui::MainWindow *ui;
};

extern MainWindow* mainwindow_atual;
#endif // MAINWINDOW_H
