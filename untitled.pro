#-------------------------------------------------
#
# Project created by QtCreator 2016-06-07T16:26:18
#
#-------------------------------------------------

QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = untitled
TEMPLATE = app

INCLUDEPATH += engine /usr/include/freetype2

QMAKE_CXXFLAGS += -std=c++11


SOURCES += main.cpp\
        mainwindow.cpp \
    glwidget.cpp \
    engine/Input/Input.cpp \
    engine/Logger/Logger.cpp \
    engine/MultithreadDataStructure/MultithreadMap.cpp \
    engine/MultithreadDataStructure/MultithreadQueue.cpp \
    engine/Renderer/Texture/Texture.cpp \
    engine/Renderer/Texture/TextureManager.cpp \
    engine/Renderer/Renderer.cpp \
    engine/Renderer/RenderQueue.cpp \
    engine/ResourceManager/AsyncResourceManager.cpp \
    engine/ResourceManager/DirectoryTree.cpp \
    engine/ResourceManager/FileDescription.cpp \
    engine/ResourceManager/Filesystem.cpp \
    engine/ResourceManager/GenericManager.cpp \
    engine/ResourceManager/MemoryFile.cpp \
    engine/ResourceManager/Resource.cpp \
    engine/ResourceManager/ResourceReference.cpp \
    engine/ResourceManager/TypeManager.cpp \
    engine/Scene/Controller/ControllerManager.cpp \
    engine/Scene/Controller/Interface.cpp \
    engine/Scene/Text/Font.cpp \
    engine/Scene/Text/FontManager.cpp \
    engine/Scene/Text/Text.cpp \
    engine/Scene/Timer/Timer.cpp \
    engine/Scene/Timer/TimerHandler.cpp \
    engine/Scene/Util/RelativePosUpdater.cpp \
    engine/Scene/Entity.cpp \
    engine/Scene/RegisteredEventsManager.cpp \
    engine/Scene/Scene.cpp \
    engine/Scene/Sprite.cpp \
    engine/Util/Circle.cpp \
    engine/Util/CollisionTest.cpp \
    engine/Util/Panic.cpp \
    engine/Util/Rect.cpp \
    engine/Util/Serialization.cpp \
    engine/Util/Transformation.cpp \
    engine/EngineMain.cpp \
    obj_controller.cpp \
    ponto_redimensionador.cpp \
    controlador_janela_base.cpp \
    modo_mover_objeto.cpp \
    dialogs/edittextures.cpp \
    dialogs/custom_widgets/picturebrowser.cpp

HEADERS  += mainwindow.h \
    glwidget.h \
    engine/Input/Input.h \
    engine/Input/keys.h \
    engine/Logger/Logger.h \
    engine/MultithreadDataStructure/MultithreadMap.h \
    engine/MultithreadDataStructure/MultithreadQueue.h \
    engine/Renderer/Texture/Texture.h \
    engine/Renderer/Texture/TextureManager.h \
    engine/Renderer/Renderer.h \
    engine/Renderer/RenderQueue.h \
    engine/ResourceManager/AsyncResourceManager.h \
    engine/ResourceManager/DirectoryTree.h \
    engine/ResourceManager/FileDescription.h \
    engine/ResourceManager/Filesystem.h \
    engine/ResourceManager/GenericManager.h \
    engine/ResourceManager/MemoryFile.h \
    engine/ResourceManager/Resource.h \
    engine/ResourceManager/ResourceReference.h \
    engine/ResourceManager/TypeManager.h \
    engine/Scene/Controller/ControllerManager.h \
    engine/Scene/Controller/Interface.h \
    engine/Scene/Text/Font.h \
    engine/Scene/Text/FontManager.h \
    engine/Scene/Text/Text.h \
    engine/Scene/Timer/Timer.h \
    engine/Scene/Timer/TimerHandler.h \
    engine/Scene/Util/RelativePosUpdater.h \
    engine/Scene/Entity.h \
    engine/Scene/RegisteredEventsManager.h \
    engine/Scene/Scene.h \
    engine/Scene/Sprite.h \
    engine/Util/Circle.h \
    engine/Util/CollisionTest.h \
    engine/Util/Panic.h \
    engine/Util/Rect.h \
    engine/Util/Serialization.h \
    engine/Util/Transformation.h \
    engine/EngineMain.h \
    obj_controller.h \
    ponto_redimensionador.h \
    controlador_janela_base.h \
    modo_mover_objeto.h \
    dialogs/edittextures.h \
    dialogs/custom_widgets/picturebrowser.h

LIBS += -lfreetype -lboost_system -lboost_filesystem

FORMS    += mainwindow.ui \
    dialogs/edittextures.ui

OTHER_FILES +=

RESOURCES += \
    icons.qrc
