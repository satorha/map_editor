#ifndef OBJ_CONTROLLER_H
#define OBJ_CONTROLLER_H

#include "engine/Scene/Controller/Interface.h"
#include "engine/Scene/Sprite.h"

class obj_controller : public Interface
{
public:
    obj_controller();

    virtual const char* getName();

    virtual void eventBegin();
    virtual void eventMouseButtonUp();
    virtual void eventMouseButtonDown();
    virtual void eventMoved();
    virtual void eventResized();
    virtual void eventDelete();
    virtual void eventAnyMouseUp();

    virtual void timerCallback(Timer* t);



    void ponto_redimensionador_pressionado(int i);

    Sprite* m_markers[4];

    void spawn_markers();
    void destroy_markers();

private:
    int arrastando;
    int current_timer;

    void change_size(int dx, int dy);
};

Interface* alocar_obj_controller();

#endif // OBJ_CONTROLLER_H
