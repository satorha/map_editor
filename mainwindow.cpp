#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QInputDialog>
#include "engine/Scene/Sprite.h"
#include "engine/Scene/Text/Text.h"
#include <QFileDialog>
#include <QDir>

MainWindow* mainwindow_atual = nullptr;


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    editando = nullptr;
    mainwindow_atual = this;

    ui->tableWidget->verticalHeader()->hide();
    ui->tableWidget->horizontalHeader()->hide();
}

MainWindow::~MainWindow()
{
    delete ui;
}

#include <QStringList>
#include <QInputDialog>

#include "engine/Renderer/Renderer.h"

void MainWindow::setEdit(Entity *obj)
{
    editando = obj;

    //preenche a tabela com os dados
    clearTable();

    if(editando == nullptr) return;

    //os dados comuns a toda entidade:
    addTableData(QString("Nome"), QString::fromStdString(editando->getName()));
    addTableData(QString("X"), QString::number(editando->getArea().getX()));
    addTableData(QString("Y"), QString::number(editando->getArea().getY()));
    addTableData(QString("W"), QString::number(editando->getArea().getW()));
    addTableData(QString("H"), QString::number(editando->getArea().getH()));
    addTableData(QString("Layer"), QString::number(editando->getLayer()));
    addTableData(QString("Alpha"), QString::number(editando->getAlpha()));

    //adiciona as coisas especificas de cada tipo
    switch(editando->getType())
    {
        case OBJ_SPRITE:
        {
            addTableData(QString("Textura"), QString("...")); //criar caixa de diálogo que edita as texturas utilizadas
            break;
        }
        case OBJ_TEXT:
        {
            Text* atual = static_cast<Text*>(editando);
            addTableData(QString("Fonte"), QString::fromStdString(atual->getFontFilename())); //criar caixa de diálogo que edita as texturas utilizadas
            addTableData(QString("Texto"), QString::fromStdString(atual->getOriginalText())); //criar caixa de diálogo que edita as texturas utilizadas
            break;
        }

        default:

            break;

    }

}

void MainWindow::clearTable()
{
    while(ui->tableWidget->rowCount() > 0)
        ui->tableWidget->removeRow(0);
}

void MainWindow::addTableData(QString nome1, QString nome2)
{
    int pos = ui->tableWidget->rowCount();
    ui->tableWidget->setRowCount(pos+1);
    ui->tableWidget->setColumnCount(2);

    ui->tableWidget->setItem(pos, 0, new QTableWidgetItem(nome1));
    ui->tableWidget->setItem(pos, 1, new QTableWidgetItem(nome2));

}

//Add obj
void MainWindow::on_actionAdicionar_objeto_triggered()
{
    QStringList lista_tipos;
    for(int i=0; i < OBJ_TYPE_Q; i++)
            lista_tipos.append(QString(GetTypeString(i)));

    bool ok;
    QString item = QInputDialog::getItem(this, tr("Selecione o tipo de objeto."),
                                             tr("Tipo:"), lista_tipos, 0, false, &ok);
    if(!ok) return;

    QString name = QInputDialog::getText(this, tr("Nome"), tr("Nome para o objeto:"), QLineEdit::Normal, QString(), &ok);
    if(!ok) return;


    Scene* cena = ui->widget->_ena_ativa;
    Entity* obj_adicionado = cena->createObject(name.toAscii(), lista_tipos.indexOf(item), "obj_controller");

    cena->setObjPos(obj_adicionado, Renderer::cameraRect().getX(), Renderer::cameraRect().getY());

}

void MainWindow::on_tableWidget_itemDoubleClicked(QTableWidgetItem *item)
{

}

void MainWindow::prompt_data_for_entity(QString data)
{
    if(data == "Layer")
    {
        bool ok = false;
        int c = QInputDialog::getInt(this, "Input...", "Camada:", editando->getLayer(), 0, 8, 1, &ok); //TODO: validar range da entrada.
        if(!ok) return;
        editando->setLayer(c);
    }
    else if(data == "Alpha")
    {
        bool ok = false;
        float c = QInputDialog::getDouble(this, "Input...", "Transparencia:", editando->getAlpha(), 0, 1, 2, &ok);
        if(!ok) return;
        editando->setAlpha(c);
    }
    else if(data == "Fonte")
    {
        Text* atual = static_cast<Text*>(editando);
        QString caminho = QFileDialog::getOpenFileName(this, "Selecione uma fonte", QString(), "TrueType font (*.ttf);;");
        if(caminho.isEmpty()) return;
        atual->setFont(QDir::current().relativeFilePath(caminho).toAscii());

    }
    else if(data == "Texto")
    {
        Text* atual = static_cast<Text*>(editando);
        bool ok = false;
        QString texto = QInputDialog::getText(this, "Input...", "Texto:", QLineEdit::Normal, QString::fromUtf8(atual->getOriginalText().c_str()), &ok);
        if(!ok) return;
        atual->setText(texto.toAscii());

    }
    else if(data == "Textura")
    {
        //criar dialogo que lista as texturas
    }
    //atualiza a tabela
    setEdit(editando);
}

void MainWindow::on_tableWidget_cellDoubleClicked(int row, int column)
{
    prompt_data_for_entity(ui->tableWidget->item(row, 0)->text());
}

void MainWindow::on_actionInserir_sprite_triggered()
{
    Scene* cena = ui->widget->_ena_ativa;
    Entity* obj_adicionado = cena->createObject(nullptr, OBJ_SPRITE, "obj_controller");

    cena->setObjPos(obj_adicionado, Renderer::cameraRect().getX(), Renderer::cameraRect().getY());
}

void MainWindow::on_actionAdicionar_texto_triggered()
{
    Scene* cena = ui->widget->_ena_ativa;
    Entity* obj_adicionado = cena->createObject(nullptr, OBJ_TEXT, "obj_controller");

    cena->setObjPos(obj_adicionado, Renderer::cameraRect().getX(), Renderer::cameraRect().getY());
}

#include "dialogs/edittextures.h"
void MainWindow::on_actionMover_Objeto_triggered()
{
    EditTextures teste(this);
    teste.exec();
}
