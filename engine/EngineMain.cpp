#include "EngineMain.h"

#include "ResourceManager/Filesystem.h"
#include "Input/Input.h"
#include "ResourceManager/AsyncResourceManager.h"
#include "Renderer/Renderer.h"
#include "Logger/Logger.h"


#define LOG() Logger::Log("Main")

void startup()
{
    Filesystem::addDiskPath("."); //configura o gerenciador de arquivos

    Input::init();
    AsyncResourceManager::init(); //inicia a thread de IO
    Renderer::init(1280,720,1280,720); //cria a janela

    registrar_controladores();
}

void quit()
{
    Input::quit();
}


Scene* _cena_ativa = nullptr;

Scene* _prox_cena = nullptr; // quando diferente de null, a cena ativa será trocada por _prox_cena

#include <fstream>
/*
    Espera até que _cena_ativa termine de carregar os recursos necessários.
*/
void wait_for_scene()
{
    if(_cena_ativa == nullptr)
        return;
    while(!_cena_ativa->finishedLoading())
    {
        Renderer::delay(2);
        managers_logic();
    }
    LOG() << "Carregamento completo.";
}

void change_scene(Scene* nova_cena)
{
    LOG() << "Trocando de cena...";
    _prox_cena = nova_cena;
}

void _change_scene()
{
    delete _cena_ativa;
    _cena_ativa = _prox_cena;
    _prox_cena = nullptr;
    LOG() << "Cena trocada.";
}

Scene* get_current_scene()
{
    return _cena_ativa;
}

Scene* alloc_scene()
{
    return new Scene();
}

void main_loop_iteraction()
{
    if(_prox_cena != nullptr)
        _change_scene();

    wait_for_scene();
    _cena_ativa->logic();
    _cena_ativa->draw();

    managers_logic();
    Renderer::update();
}

void quit_engine()
{
    delete _cena_ativa;
    _cena_ativa = nullptr;

    end_io_thread();
}
