#include "Transformation.h"
#include <cmath>

Transformation::Transformation(float rotacao) :
    m_matrix{0, 0, 0,
             0, 0, 0,
             0, 0, 1}
{
    const float cosseno = cos(rotacao);
    const float seno = sin(rotacao);

    m_matrix[0] = cosseno;
    m_matrix[1] = -seno;
    m_matrix[3] = seno;
    m_matrix[4] = cosseno;

}

Transformation::Transformation(float dx, float dy):
    m_matrix{1, 0, dx,
             0, 1, dy,
             0, 0, 1}
{

}

void Transformation::transform(float& x, float& y) const
{
    const int _x=x;
    x = _x*m_matrix[0] + y*m_matrix[1] + m_matrix[2];
    y = _x*m_matrix[3] + y*m_matrix[4] + m_matrix[5];
}


Transformation::~Transformation()
{
    //dtor
}
