#ifndef COLLISIONTEST_H
#define COLLISIONTEST_H

#include "Rect.h"
#include "Circle.h"



bool collisionTest(const Rect& r1, const Rect& r2);
bool collisionTest(const Rect& r1, const Circle& c2);
bool collisionTest(const Circle& c1, const Circle& c2);


#endif // COLLISIONTEST_H
