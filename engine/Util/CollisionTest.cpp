#include "CollisionTest.h"
#include <algorithm>
#include <cstring>


bool collisionTest(const Rect& r1, const Rect& r2)
{
    const int raio_1 = (std::max(r1.getW(),r1.getH()) + std::max(r2.getW(), r2.getH()))*1.4142/2;

    const int dx = r1.getX()-r2.getX();
    const int dy = r1.getY()-r2.getY();

    const int dist =  (dx*dx + dy*dy);

    if(dist > (raio_1*raio_1)) return false; // está longe demais. a colisão é impossível


//    const int menor_1 = std::min(r2.getW(), r2.getH())/2;
    const int menor_2 = std::min(r1.getW(), r1.getH())/2;
    const int raio_2 = menor_2;

    if(dist < raio_2*raio_2)
        return true; //está perto demais. a colisão é certeza


    double planos[4][2]; //4 planos, 2 coordenadas(x e y)

    planos[0][0] = (cos(r1.getRotation()));
    planos[0][1] = (sin(r1.getRotation()));

    planos[1][0] = (-sin(r1.getRotation()));
    planos[1][1] = (cos(r1.getRotation()));

    planos[2][0] = (cos(r2.getRotation()));
    planos[2][1] = (sin(r2.getRotation()));

    planos[3][0] = (-sin(r2.getRotation()));
    planos[3][1] = (cos(r2.getRotation()));




    float pontos[16]; //os primeiros 8 pontos sao do primeiro retangulo e os outros 8 do segundo


    r1.getPoints(pontos);
    r2.getPoints(&pontos[8]);
    //aplica uma transformacao para que os retangulos fiquem proximos a origem.
    //isto para evitar overflow quando estiverem distantes da mesma

    for(int i=0; i < 4; i++)
    {
        pontos[2*i] -= r1.getX();
        pontos[2*i+1] -= r1.getY();

        pontos[2*i+8] -= r1.getX();
        pontos[2*i+1+8] -= r1.getY();
    }
    for(int i=0; i < 4; i++)
    {
        //para o plano atual, plotar os pontos maximo e minimo de cada retangulo
        //o retangulo 0 e o this
        //o retangulo 1 e o r2
        double minimo[2];
        double maximo[2];
        for(int k=0; k < 2; k++)
        {

            double projetados[4];
            for(int j=0; j<4; j++) //faz a projecao do ponto j
                projetados[j] = pontos[2*j+k*8]*planos[i][0] + pontos[2*j+1+k*8]*planos[i][1];

            minimo[k] = projetados[0];
            for(int j=1; j < 4; j++)
                minimo[k] = std::min(minimo[k], projetados[j]);

            maximo[k] = projetados[0];
            for(int j=1; j < 4; j++)
                maximo[k] = std::max(maximo[k], projetados[j]);
        }

        if(minimo[0] < minimo[1])
        {
            if(abs(minimo[0] - minimo[1]) > (maximo[0]-minimo[0])) return false;
        }
        else
        {
            if(abs(minimo[1] - minimo[0]) > (maximo[1]-minimo[1])) return false;
        }
    }

    return true;
}


bool collisionTest(const Circle& c1, const Circle& c2)
{
    const int dx = c1.x - c2.x;
    const int dy = c1.y - c2.y;
    const int dist2 =  dx*dx + dy*dy;

    int dist_max2 = c1.r+c2.r;
    dist_max2 *= dist_max2;

    if(dist2 < dist_max2) return true;

    return false;
}



bool collisionTest(const Rect& r2, const Circle& c1)
{
    const int raio_1 = c1.r + std::max(r2.getW(), r2.getH())*1.4142;

    const int dx = c1.x-r2.getX();
    const int dy = c1.y-r2.getY();
    if((dx*dx + dy*dy) > (raio_1*raio_1)) return false;

    //return true;

    double planos[2][2]; //4 planos, 2 coordenadas(x e y)

    planos[0][0] = (cos(r2.getRotation()));
    planos[0][1] = (sin(r2.getRotation()));

    planos[1][0] = (-sin(r2.getRotation()));
    planos[1][1] = (cos(r2.getRotation()));


    float pontos[8]; //os primeiros 8 pontos sao do primeiro retangulo e os outros 8 do segundo
    r2.getPoints(pontos);


    //aplica uma transformacao para que os retangulos fiquem proximos a origem.
    //isto para evitar overflow quando estiverem distantes da mesma

    for(int i=0; i < 4; i++)
    {
        pontos[2*i] -= c1.x;
        pontos[2*i+1] -= c1.y;
    }

    for(int i=0; i < 2; i++)
    {
        //para o plano atual, plotar os pontos maximo e minimo de cada retangulo
        //o retangulo 0 e o this
        //o retangulo 1 e o r2
        double minimo_ret;
        double maximo_ret;

        double minimo_circ = -c1.r;
        double maximo_circ = +c1.r;


        double projetados[4];
        for(int j=0; j<4; j++) //faz a projecao do ponto j
            projetados[j] = pontos[2*j]*planos[i][0] + pontos[2*j+1]*planos[i][1];

        minimo_ret = projetados[0];
        for(int j=1; j < 4; j++)
            minimo_ret = std::min(minimo_ret, projetados[j]);

        maximo_ret = projetados[0];
        for(int j=1; j < 4; j++)
            maximo_ret = std::max(maximo_ret, projetados[j]);


        if(minimo_ret < minimo_circ)
        {
            if(abs(minimo_ret - minimo_circ) > (maximo_ret-minimo_ret)) return false;
        }
        else
        {
            if(abs(minimo_circ - minimo_ret) > (maximo_circ-minimo_circ)) return false;
        }
    }

    return true;
}
