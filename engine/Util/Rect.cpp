#include "Rect.h"

#include <cmath>
#include "Transformation.h"


Rect::Rect(const float x_pos, const float y_pos, const float width, const float height)
{
    x = x_pos;
    y = y_pos;
    w = width;
    h = height;
    rotacao = 0;
}

void Rect::getPoints(float* pontos) const
{
    const float a = h*0.5;
    const float b = w*0.5;

    //inicialmente isto e um retangulo centralizado na origem
    pontos[0] = -b;
    pontos[1] = -a;
    pontos[2] = b;
    pontos[3] = -a;
    pontos[4] = b;
    pontos[5] = a;
    pontos[6] = -b;
    pontos[7] = a;


    //aplica rotacao e depois a translação
    for(int i=0; i < 4; i++)
    {
        // TODO: fazer rotacao
        //rot_t.transform(pontos[2*i], pontos[2*i+1]);
        pontos[2*i] += x;
        pontos[2*i+1] += y;
    }
}

#include <iostream>
bool Rect::isInside(const Rect& outro) const
{
    const float esquerda = x - w/2;
    const float esquerda_outro = outro.x - outro.w/2;

    const float topo = y - h/2;
    const float topo_outro = outro.y - outro.h/2;

    //Para estar dentro de outro, a esquerda deste deve estar à direito da esquerda de outro.
    if(esquerda < esquerda_outro) return false;

    //o topo deste deve estar abaixo do topo do outro
    if(topo < topo_outro) return false;

    //a direita deste deve estar à esquerda da direita do outro
    if((esquerda+w) > (esquerda_outro + outro.w)) return false;

    //o fundo deste deve estar acimado fundo do outro
    if((topo+w) > (topo_outro + outro.h)) return false;

    return true; //está dentro do outro
}




