#ifndef RECT_H
#define RECT_H

#include "Transformation.h"
#include "Circle.h"
class Rect
{
public:
    Rect(const float x_pos=0, const float y_pos=0, const float width=0, const float height=0);

    inline void setX(const float a)
    {
        x = a;
    }
    inline void setY(const float a)
    {
        y = a;
    }
    inline void setW(const float a)
    {
        w = a;
    }
    inline void setH(const float a)
    {
        h = a;
    }
    inline void setXandY(float x1, float y1)
    {
        x = x1;
        y = y1;
    }
    inline void setRotation(const float a)
    {
        rotacao = a;
    }

    inline float getX() const
    {
        return x;
    }

    inline float getY() const
    {
        return y;
    }
    inline float getW() const
    {
        return w;
    }
    inline float getH() const
    {
        return h;
    }
    inline float getRotation() const
    {
        return rotacao;
    }

    inline void sum(const float a, const float b)
    {
        x += a;
        y += b;
    }

    inline void multiply(const float a, const float b)
    {
        x *= a;
        y *= b;
        w *= a;
        h *= b;
    }

    inline void sumRotation(float f)
    {
        rotacao += f;
    }

    void getPoints(float* out) const;
    inline void resize(float new_w, float new_h)
    {
        w = new_w;
        h = new_h;
    }

    /*
        Retorna true se este retangulo estiver dentro de outro.

        Este método assume que os retângulos não estão rotacionados.
    */
    bool isInside(const Rect& outro) const;

private:
    float x, y, w, h; //x e y sao o centro
    float rotacao;



};

#endif // RECT_H
