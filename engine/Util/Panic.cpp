#include "Panic.h"
#include "Logger/Logger.h"
#include <string>

#define LOG() Logger::Log("Panic")


bool fatalError = false;

void Panic(const char* str)
{
    fatalError = true;
    LOG() << std::string(str);
    LOG() << std::string("O programa será fechado.");
    exit(-1);
}
