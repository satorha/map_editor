#ifndef PANIC_H
#define PANIC_H


/*
    Sai do programa, imprimindo uma mensagem de erro para o log.
*/
void Panic(const char* str);

bool fatalErrorOcurred();

#endif // PANIC_H
