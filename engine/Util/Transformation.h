#ifndef TRANSFORMATION_H
#define TRANSFORMATION_H


class Transformation
{
    public:
        explicit Transformation(float rotation); //cria uma transformação de rotação
        explicit Transformation(float dx, float dy); //uma transformação de rotação

        //aplica uma transformacao em um vetor (x,y)
        void transform(float& x, float& y) const;

        virtual ~Transformation();

        float getRotation() const
        {
            return rot;
        }
        float getX() const
        {
            return dx;
        }
        float getY() const
        {
            return dy;
        }
    private:
        float m_matrix[9]; //matriz 3x3
        float rot;
        float dx;
        float dy;
};

#endif // TRANSFORMATION_H
