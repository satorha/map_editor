#include "Serialization.h"

#include <cstring>

template<>
void write_var_to_file<std::string>(const std::string& var, std::ostream& out)
{
    uint32_t tamanho = var.size();
    write_var_to_file(tamanho, out);

    out.write(var.c_str(), tamanho);
}

//ler primeiro o tamanho da string e depois os caracteres
template<>
void read_var_from_bin<std::string>(std::string& out,  MemoryFile* f)
{
    uint32_t tamanho;
    read_var_from_bin(tamanho, f);

    char buffer[tamanho+2];
    buffer[tamanho] = 0;
    f->read(buffer, tamanho);

    out = std::string(buffer);
}

template<>
void write_var_to_file<Rect>(const Rect& var, std::ostream& out)
{
    write_var_to_file(var.getX(), out);
    write_var_to_file(var.getY(), out);
    write_var_to_file(var.getW(), out);
    write_var_to_file(var.getH(), out);
    write_var_to_file(var.getRotation(), out);
}

template<>
void read_var_from_bin<Rect>(Rect& out, MemoryFile* f)
{
    float x, y, w, h, rotation;
    read_var_from_bin(x, f);
    read_var_from_bin(y, f);
    read_var_from_bin(w, f);
    read_var_from_bin(h, f);
    read_var_from_bin(rotation, f);

    out = Rect(x, y, w, h);
    out.setRotation(rotation);

}
