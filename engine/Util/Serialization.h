#ifndef SERIALIZATION_H
#define SERIALIZATION_H
#include <ostream>
#include <string>
#include "Rect.h"
#include <cstring>
#include "ResourceManager/MemoryFile.h"

/*
    Este arquivo contém um conjunto de funções pra facilitar a escrita de arquivos binários.
*/


template <class T>
void write_var_to_file(const T& var, std::ostream& out)
{
    out.write((char*)&var, sizeof(T));
}
template<>
void write_var_to_file<std::string>(const std::string& var, std::ostream& out);

template<>
void write_var_to_file<Rect>(const Rect& var, std::ostream& out);



template <class T>
void read_var_from_bin(T& out, MemoryFile* f)
{
    f->read((char*)&out, sizeof(T));
}

template<>
void read_var_from_bin<std::string>(std::string& out, MemoryFile* f);

template<>
void read_var_from_bin<Rect>(Rect& out, MemoryFile* f);

#endif // SERIALIZATION_H
