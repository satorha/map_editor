#include "Texture.h"

#ifdef USAR_SDL
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#endif

#include "Logger/Logger.h"
#define LOG() Logger::Log("Texture")

#include <cstring>


const float textura_coordenadas_padrao[] =
{
    0.0, 0.0,
    1.0, 0.0,
    1.0, 1.0,
    0.0, 1.0,
};

Texture::Texture(const DirectoryTree* p) :
    Resource(p)
{
    m_width = m_height = 0;
    m_openglID = -1;

    memcpy(m_texmap, textura_coordenadas_padrao, sizeof(float)*8);
}

Texture::~Texture()
{

}

#include <QImage>
void Texture::onDataAvailable(std::vector<char>& data)
{
#ifdef USAR_SDL
    //arquivo lido. agora extrai os pixels
    SDL_RWops *rw;
    rw = SDL_RWFromMem(&data[0], data.size());
    SDL_Surface *img = IMG_Load_RW(rw, 1);
    if(img == NULL)
        return; //algum erro ao carregar a imagem


    //"otimiza" a imagem para o modo de video atual
    //SDL_Surface* otimizada = SDL_CreateRGBSurface(0,img->w,img->h,32, rmask, gmask, bmask, amask);
    //SDL_BlitSurface(img, NULL, otimizada, NULL);
    SDL_Surface* otimizada = SDL_DisplayFormatAlpha(img);

    SDL_FreeSurface(img);

    setPixelData((uint8_t*)otimizada->pixels, otimizada->w, otimizada->h);

    SDL_FreeSurface(otimizada);

#else
    QImage imagem;
    imagem.loadFromData((const uchar*)&data[0], data.size());

    setPixelData(imagem.bits(), imagem.width(), imagem.height());
#endif




}

void Texture::setPixelData(uint8_t* data, int w, int h)
{
    m_pixel_data.resize(w*h*4);
    m_width = w;
    m_height = h;
    memcpy(&m_pixel_data[0], data, w*h*4);
}

std::shared_ptr<Texture> Texture::clip(const Rect& area)
{
    if(!isReady())
    {
        LOG() << "Erro: clip(): a textura não está pronta ainda.";
        return std::shared_ptr<Texture>(nullptr);
    }

    std::shared_ptr<Texture> nova(new Texture(NULL));
    nova->m_parent = std::static_pointer_cast<Texture>(this->shared_from_this());
    nova->m_openglID = m_openglID;
    nova->m_width = area.getW();
    nova->m_height = area.getH();
    nova->setReady();


    const float w0 = m_width/(m_texmap[2] - m_texmap[0]); //w0
    const float h0 = m_height/(m_texmap[7] - m_texmap[1]); //h0


    const float inicio_x = m_texmap[0] + (area.getX()/w0);
    const float inicio_y = m_texmap[1] + (area.getY()/h0);

    const float larg = area.getW()/w0;
    const float alt = area.getH()/h0;

    const float vetores[] = {
         0, 0,
         1, 0,
         1, 1,
         0, 1,
    };
    for(int i=0; i < 4; i++)
    {
        nova->m_texmap[2*i] = inicio_x + (vetores[2*i]*larg);
        nova->m_texmap[2*i+1] = inicio_y + (vetores[2*i+1]*alt);
    }

    return nova;
}
