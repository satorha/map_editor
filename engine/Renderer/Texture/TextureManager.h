#ifndef TEXTUREMANAGER_H
#define TEXTUREMANAGER_H

#include "ResourceManager/TypeManager.h"
#include "Texture.h"

#include <map>

class TextureManager : public TypeManager
{
public:
    TextureManager();
    virtual ~TextureManager();

    bool processItem(std::shared_ptr<Resource> res);
    int eraseUnused();

    /*
        Cria uma textura a partir de dados de pixel na memória.

        Como não é necessária a leitura de um arquivo, este novo objeto não vai passar pela thread de IO,
        indo direto pra fila de processamento final da TypeManager.

        Nenhum nome é definido para o recurso criado. Por causa disto, ele é armazenado em um índice diferente internamente.

        O formato esperado da imagem é RGBA

        se W e H forem zero, retorna nullptr
    */
    std::shared_ptr<Texture> createTexture(uint8_t* pixels, int w, int h);
protected:

    std::shared_ptr<Resource> alloc(const char* filename);

    void store_ptr(std::shared_ptr<Resource> res);
    std::shared_ptr<Resource> find_ptr(const char* filename);

private:

    std::map<std::string, std::shared_ptr<Texture> > m_texturas;

    std::vector<std::weak_ptr<Texture> > m_runtime_textures;
};

extern TextureManager texManager;
#endif // TEXTUREMANAGER_H
