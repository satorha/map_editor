#include "TextureManager.h"
#include "Logger/Logger.h"
#include "Renderer/Renderer.h"
#define LOG() Logger::Log("TextureManager")
#include "ResourceManager/Filesystem.h"

TextureManager texManager;

TextureManager::TextureManager()
{
    //ctor
}

TextureManager::~TextureManager()
{
    LOG() << "Desligando.";
}

bool TextureManager::processItem(std::shared_ptr<Resource> res)
{
    std::shared_ptr<Texture> atual = std::static_pointer_cast<Texture>(res);
    LOG() << "Processando textura \"" << atual->getFullPath() << "\" (" << atual->getW() << "x" << atual->getH() << ")";

    std::vector<char>& vetor = atual->m_pixel_data;
    if(vetor.size() == 0)
        return false;

    atual->m_openglID = Renderer::uploadPixels(atual->getW(), atual->getH(), &vetor[0]);

    LOG() << "\"" << atual->getFullPath() << "\" processada com sucesso.";
    return true;
}

int TextureManager::eraseUnused()
{
    LOG() << "Apagando texturas não utilizadas.";
    //TODO: implementar este método
    //TODO: apagar a lista de weak_ptr
    return 0;
}

std::shared_ptr<Resource> TextureManager::alloc(const char* filename)
{
    const DirectoryTree* path;
    if(filename != nullptr)
        path = Filesystem::getNode(filename);
    else path = nullptr;

    return std::shared_ptr<Resource>(new Texture(path));
}


void TextureManager::store_ptr(std::shared_ptr<Resource> res)
{
    m_texturas[res->getName()] = std::static_pointer_cast<Texture>(res);
}

std::shared_ptr<Resource> TextureManager::find_ptr(const char* filename)
{
    auto it = m_texturas.find(filename);
    if(it == m_texturas.end())
        return nullptr;

    return it->second;
}

std::shared_ptr<Texture> TextureManager::createTexture(uint8_t* pixels, int w, int h)
{
    if(w == 0 || h == 0)
        return nullptr;
    std::shared_ptr<Texture> nova = std::static_pointer_cast<Texture>(alloc(nullptr));

    nova->setPixelData(pixels, w, h);

    addToQueue(std::static_pointer_cast<Resource>(nova));


    /*
        Coloca na lista de carregados (talvez debugar mais tarde?)
    */
    m_runtime_textures.push_back(std::weak_ptr<Texture>(nova));

    return nova;
}
