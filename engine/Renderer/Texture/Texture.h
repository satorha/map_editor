#ifndef TEXTURE_H
#define TEXTURE_H

#include "ResourceManager/Resource.h"
#include <memory>
#include "Util/Rect.h"

class Texture : public Resource
{
public:
    explicit Texture(const DirectoryTree* p);
    virtual ~Texture();


    void onDataAvailable(std::vector<char>& data);

    /*
        Cria uma nova textura usando a atual como base.

        Só pode ser utilizada se a textura base já tiver sido completamente carregada (flag ready setada)
    */
    std::shared_ptr<Texture> clip(const Rect& area);

    int getW()
    {
        return m_width;
    }
    int getH()
    {
        return m_height;
    }

    const float* getCoords()
    {
        return m_texmap;
    }
    int getOGLid()
    {
        if(m_openglID != -1) return m_openglID;

        if(m_parent != nullptr)
            return m_openglID = m_parent->getOGLid();

        return -1;
    }

    /*
        Usada pela onDataAvailable para especificar a imagem decodificada.
    */
    void setPixelData(uint8_t* data, int w, int h);

protected:

private:
    friend class TextureManager;
    std::shared_ptr<Texture> m_parent;

    std::vector<char> m_pixel_data;
    int m_width;
    int m_height;
    int m_openglID;
    float m_texmap[8];
};

#endif // TEXTURE_H
