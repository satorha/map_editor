#ifndef RENDERER_H
#define RENDERER_H

#include "Util/Rect.h"
#include "RenderQueue.h"

#define LAYER_AMOUNT 5


class Texture;
class Renderer
{
public:

    /*
        inicializa o opengl e toda as coisas graficas
    */
    static bool init(int world_w, int world_h, int window_w, int window_h);

    static void quit();

    /*
        Desenha um retangulo preenchido com alguma textura.
        Ele vai fazer parte do frame no proximo update()

    */
    static void drawQuad(Rect ret, Texture* tex, const float alpha, const int layer, const float r=1, const float g=1, const float b=1);

    /*
        Desenha o conteúdo de todas as filas de desenho.
    */
    static void update();

    /*
        Retorna um retangulo representando a posicao atual da câmera
    */
    static const Rect& cameraRect();

    /*
        Funções de manipulação da câmera
    */
    static void moveCamera(int dx, int dy);

    static void setCameraPos(int x, int y);

    static void resizeCamera(int w, int h);

    static int uploadPixels(int w, int h, void* data);

    static void delay(int ms);

    static int getWindowWidth();
    static int getWindowHeight();

    static void window_resized(int w, int h);


private:

    Renderer();
};

#endif // RENDERER_H
