#ifndef RENDERQUEUE_H
#define RENDERQUEUE_H


#include <vector>
#include "Texture/Texture.h"
#include "Util/Rect.h"
//#include "Texture.h"
#include <cstring>


class RenderQueue
{
public:
    RenderQueue();
    ~RenderQueue();


    void queue(const Rect& area, Texture* tex, float alpha, float r=1, float g=1, float b=1)
    {
        float pontos[8];
        area.getPoints(pontos);
        if(tex != nullptr)
            m_queue.push_back(renderCommand(pontos, tex->getCoords(), alpha, tex->getOGLid(), r, g, b));
        else
            m_queue.push_back(renderCommand(pontos, nullptr, alpha, -1, r, g, b));
    }

    struct renderCommand
    {
        renderCommand(const float* points, const float* tex_coord, float alpha, int id, float _r, float _g, float _b)
        {
            m_tex_id = id;
            memcpy(m_points, points, sizeof(float)*8);

            if(tex_coord != nullptr)
                memcpy(m_tex_coord, tex_coord, sizeof(float)*8);

            m_alpha = alpha;

            r = _r;
            g = _g;
            b = _b;
        }
        float m_points[8];
        float m_tex_coord[8];
        int m_tex_id;
        float m_alpha;
        float r;
        float g;
        float b;
    };


    void draw();
private:
    std::vector<renderCommand> m_queue;

};

#endif // RENDERQUEUE_H
