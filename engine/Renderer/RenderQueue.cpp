#include "RenderQueue.h"
#include <GL/gl.h>


RenderQueue::RenderQueue()
{
    m_queue.reserve(16);

}

RenderQueue::~RenderQueue()
{
    //dtor
}

void RenderQueue::draw()
{
    glEnableClientState(GL_VERTEX_ARRAY);
    for(renderCommand& atual : m_queue)
    {
        glColor4f(atual.r, atual.g, atual.b, atual.m_alpha);
        glBindTexture(GL_TEXTURE_2D, atual.m_tex_id);
        glVertexPointer(2, GL_FLOAT, 0, atual.m_points);
        glTexCoordPointer(2, GL_FLOAT, 0, atual.m_tex_coord);
        glDrawArrays(GL_QUADS, 0, 4);
    }
    glDisableClientState(GL_VERTEX_ARRAY);
    m_queue.clear();
}

