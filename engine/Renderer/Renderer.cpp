#include "Renderer.h"
#include "Logger/Logger.h"
#define LOG() Logger::Log("Renderer")

#ifdef USAR_SDL
#include <SDL/SDL.h>
#endif

#include <GL/gl.h>



RenderQueue m_layer[LAYER_AMOUNT];

Rect _camera;
int _window_w;
int _window_h;

int Renderer::getWindowWidth()
{
    return _window_w;
}

int Renderer::getWindowHeight()
{
    return _window_h;
}

bool init_window(int w, int h)
{
    _window_w = w;
    _window_h = h;


    _camera.sum(w/2, h/2);
#ifdef USAR_SDL
    //Initialize SDL
    if( SDL_Init( SDL_INIT_VIDEO ) < 0 )
    {
        return false;
    }

    //Create Window
    SDL_Surface* video;
    video = SDL_SetVideoMode( w, h, 32, SDL_OPENGL );
    if(video == NULL )
    {
        return false;
    }
#endif

    return true;

}

bool init_opengl()
{
    glClearColor( 0, 0, 0, 0 );
    glDisable( GL_DEPTH_TEST );
    glDisable( GL_LIGHTING );
    glEnable(GL_CULL_FACE);
    glCullFace(GL_FRONT);
    glEnable(GL_TEXTURE_2D);
    glEnable (GL_BLEND);
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);


    //If there was any errors
    if( glGetError() != GL_NO_ERROR )
    {
        return false;
    }



    return true;
}

void resize_world(int w, int h)
{
    _camera.resize(w, h);


    //Set projection
    glMatrixMode( GL_PROJECTION );
    glLoadIdentity();
    glOrtho( -w/2, w/2, h/2, -h/2, -1, 800 );
}

#include "Input/Input.h"
void Renderer::window_resized(int w, int h)
{
    glViewport(0, 0,  w,  h);
    _window_w = w;
    _window_h = h;
}

bool Renderer::init(int world_w, int world_h, int window_w, int window_h)
{
    LOG() << "Iniciando Renderizado com " << LAYER_AMOUNT << " camadas.";
    if(!init_window(window_w, window_h))
    {
        LOG() << "Erro ao criar janela.";
        return false;
    }
    if(!init_opengl())
    {
        LOG() << "Erro ao iniciar opengl.";
        return false;
    }
    resize_world(world_w, world_h);

    LOG() << "Iniciado com sucesso.";

    return true;

}

void Renderer::drawQuad(Rect ret, Texture* tex, const float alpha, const int layer, const float r, const float g, const float b)
{
    //Aplica a transformação causada pela câmera
    ret.sum(-_camera.getX(), -_camera.getY());

    m_layer[layer].queue(ret, tex, alpha, r, g, b);
}

void Renderer::update()
{
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);

    //terminou de definir os pontos. renderiza a cena.
    glClear(GL_COLOR_BUFFER_BIT);

    for(RenderQueue& camada_atual : m_layer)
    {
        camada_atual.draw();
    }


    glDisableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);

#ifdef USAR_SDL
    SDL_GL_SwapBuffers();
#endif
}

int Renderer::uploadPixels(int w, int h, void* data)
{
    GLuint id = 0;

    glGenTextures(1, &id);
    glBindTexture(GL_TEXTURE_2D, id);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST); //texture filter
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST); //texture filter
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_BGRA, GL_UNSIGNED_BYTE, data); //copy the image to the texture


    int err = glGetError();
    if(err != GL_NO_ERROR)
    {
        LOG() << "Erro no upload da textura.";
        return -1;
    }

    return (int)id;
}

const Rect& Renderer::cameraRect()
{
    return _camera;
}

void Renderer::moveCamera(int dx, int dy)
{
    _camera.sum(dx, dy);
}

void Renderer::setCameraPos(int x, int y)
{
    _camera.setX(x);
    _camera.setY(y);
}

void Renderer::resizeCamera(int w, int h)
{
    resize_world(w, h);
}

void Renderer::quit()
{
#ifdef USAR_SDL
    SDL_Quit();
#endif
}

void Renderer::delay(int ms)
{
#ifdef USAR_SDL
    SDL_Delay(ms);
#endif
}

