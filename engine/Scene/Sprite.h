#ifndef SPRITE_H
#define SPRITE_H

#include "Entity.h"
#include "Renderer/Texture/Texture.h"
#include <ctype.h>

class Sprite : public Entity
{
    public:
        Sprite(const DirectoryTree* name);
        virtual ~Sprite();

        struct frame
        {
            std::shared_ptr<Texture> tex;
            std::vector<hitbox_t> hitboxes;
            int delay;
        };

        const std::vector<hitbox_t>& getHitboxes();

        void draw();

        void onDataAvailable(std::vector<char>& data);

        int addFrame(frame& f);
        void setCurrentFrame(int id)
        {
            frame_atual = id;
        }

        bool saveToFile(std::ostream& out);
        bool loadFromData(MemoryFile* f);

        /*
            Carrega as texturas indicadas por m_textures_to_request.
        */
        bool loadDependencies();

        OBJ_TYPE getType();

        void setColor(uint8_t r, uint8_t g, uint8_t b);


    private:
        std::vector<hitbox_t> m_backup;
        struct frame_preload
        {
            std::string tex_name;
            std::vector<hitbox_t> hitboxes;
            int delay;
        };
        std::vector<frame_preload> m_textures_to_request; //veja o método loadDependencies

        std::vector<frame> m_frames;
        int frame_atual;

        float r;
        float g;
        float b;
};

#endif // SPRITE_H
