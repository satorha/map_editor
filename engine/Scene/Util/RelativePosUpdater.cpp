#include "RelativePosUpdater.h"

RelativePosUpdater::RelativePosUpdater(Scene* cena, Entity* master, Entity* slave, float dx, float dy)
{
    m_scene = cena;
    m_master = master;
    m_slave = slave;
    m_dx = dx;
    m_dy = dy;
}

RelativePosUpdater::~RelativePosUpdater()
{
    //dtor
}

void RelativePosUpdater::update()
{
    m_scene->setObjPos(m_slave, m_master->getX()+m_dx, m_master->getY()+m_dy);
}
