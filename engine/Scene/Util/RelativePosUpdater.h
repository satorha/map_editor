#ifndef RELATIVEPOSUPDATER_H
#define RELATIVEPOSUPDATER_H

#include "Scene/Scene.h"


class RelativePosUpdater
{
public:
    RelativePosUpdater(Scene* cena, Entity* master, Entity* slave, float dx, float dy);
    ~RelativePosUpdater();

    void update();

private:
    Entity* m_master;
    Entity* m_slave;
    Scene* m_scene;
    float m_dx;
    float m_dy;
};

#endif // RELATIVEPOSUPDATER_H
