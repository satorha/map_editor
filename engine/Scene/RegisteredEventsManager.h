#ifndef REGISTEREDEVENTSMANAGER_H
#define REGISTEREDEVENTSMANAGER_H

#include "Controller/Interface.h"
#include <vector>
class Scene;

enum EVENT_TYPE
{
    EVENT_BEGIN,
    EVENT_MOVED,
    EVENT_MOUSEUP,
    EVENT_MOUSEDOWN,
    EVENT_FRAME,
    EVENT_KEYDOWN,
    EVENT_KEYUP,
    EVENT_CAMERAUPDATE,
    EVENT_RESIZED,
    EVENT_ANYMOUSEUP,
    EVENT_DELETE
};


const int EVENTOS_REGISTRAVEIS_QUANT = 5;

class RegisteredEventsManager
{
public:
    RegisteredEventsManager();
    ~RegisteredEventsManager();


    void registerEvent(int type, Interface* control);

    void deregisterEvent(int type, Interface* control);

    const std::vector<Interface*>& getQueue(int type)
    {
        return *_getQueue(type);
    }

private:

    std::vector<Interface*>* _getQueue(int type);

    std::vector<Interface*> m_queues[EVENTOS_REGISTRAVEIS_QUANT];
};

#endif // REGISTEREDEVENTSMANAGER_H
