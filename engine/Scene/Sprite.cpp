#include "Sprite.h"
#include "Renderer/Renderer.h"
#include "Util/Serialization.h"
#include "Renderer/Texture/TextureManager.h"

#include "Logger/Logger.h"
#define LOG() Logger::Log("Sprite")


Sprite::Sprite(const DirectoryTree* name) :
    Entity(name)
{
    frame_atual = 0;
    r = g = b = 1;
}

Sprite::~Sprite()
{
    //dtor
}

const std::vector<Entity::hitbox_t>& Sprite::getHitboxes()
{
    if(m_frames.size() == 0)
    {
        m_backup.resize(1);
        m_backup[0].area = getArea();
        m_backup[0].name.clear();
        return m_backup;
    }
    m_frames[frame_atual].hitboxes.clear();
    hitbox_t dummy;
    dummy.area = getArea();
    m_frames[frame_atual].hitboxes.push_back(dummy);
    return m_frames[frame_atual].hitboxes;
}

void Sprite::setColor(uint8_t a, uint8_t b, uint8_t c)
{
    r = a/255.0;
    g = b/255.0;
    b = c/255.0;

}

void Sprite::draw()
{
    if(m_frames.size() != 0)
        Renderer::drawQuad(getArea(), m_frames[frame_atual].tex.get(), getAlpha(), getLayer(), r, g, b);
    else
        Renderer::drawQuad(getArea(), nullptr, getAlpha(), getLayer(), r, g, b);
}

void Sprite::onDataAvailable(std::vector<char>& data)
{
    Entity::onDataAvailable(data);
}

int Sprite::addFrame(frame& f)
{
    m_frames.push_back(std::move(f));
    setCurrentFrame(m_frames.size()-1);
    return m_frames.size()-1;
}

bool Sprite::loadDependencies()
{
    for(frame_preload& atual : m_textures_to_request)
    {
        std::cout << "dependencia " << atual.tex_name << std::endl;
        auto p = std::static_pointer_cast<Texture>(texManager.get(atual.tex_name.c_str()));
        if(p == nullptr)
            return false;
        frame novo;
        novo.tex = p;
        novo.hitboxes = std::move(atual.hitboxes);
        novo.delay = atual.delay;

        addDependency(p);
        addFrame(novo);
    }

    m_textures_to_request.clear();
    m_textures_to_request.shrink_to_fit();
    return true;
}

bool Sprite::saveToFile(std::ostream& out)
{
    Entity::saveToFile(out);

    uint8_t textures_amount = m_frames.size();
    write_var_to_file(textures_amount, out);

    for(unsigned int i=0; i < m_frames.size(); i++)
    {
        uint8_t hitbox_amount = m_frames[i].hitboxes.size();
        write_var_to_file(m_frames[i].tex->getFullPath(), out); //textura utilizada neste frame
        write_var_to_file(m_frames[i].delay, out); //tempo que este frame é exibido antes de passar pro próximo
        write_var_to_file(hitbox_amount, out); //quantidade de hitboxes neste frame

        for(hitbox_t& h : m_frames[i].hitboxes) //escreve os dados de cada uma das hitboxes
        {
            write_var_to_file(h.area, out);
            write_var_to_file(h.name, out);
        }
    }

    return true;
}

bool Sprite::loadFromData(MemoryFile* f)
{
    Entity::loadFromData(f);

    uint8_t textures_amount;
    read_var_from_bin(textures_amount, f);

    m_textures_to_request.resize(textures_amount);

    LOG() << "Carregando sprite com " << (int)textures_amount << " texturas" << ".";

   for(frame_preload& atual : m_textures_to_request)
    {
        read_var_from_bin(atual.tex_name, f);
        read_var_from_bin(atual.delay, f);
        uint8_t hitbox_amount;
        read_var_from_bin(hitbox_amount, f);

        LOG() << "Pedindo textura \"" << atual.tex_name << "\" delay:" << atual.delay << " hitbox: " << (int)hitbox_amount << ".";
        atual.hitboxes.resize(hitbox_amount);
        for(int i=0; i < hitbox_amount; i++)
        {
            read_var_from_bin(atual.hitboxes[i].area, f);
            read_var_from_bin(atual.hitboxes[i].name, f);
        }
    }
    return true;
}

OBJ_TYPE Sprite::getType()
{
    return OBJ_SPRITE;
}
