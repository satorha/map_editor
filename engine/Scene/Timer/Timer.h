#ifndef TIMER_H
#define TIMER_H

class Interface;

class Timer
{
public:
    Timer();
    ~Timer();

    void setReceptor(Interface* e)
    {
        m_receptor = e;
    }

    void setInterval(int frames)
    {
        m_frames_remaining = frames;
        m_interval = frames;
    }

    bool isActive()
    {
        return (m_frames_remaining != -1);
    }

    bool singleShot()
    {
        return m_single_shot;
    }
    void setSingleShot(bool v)
    {
        m_single_shot = v;
    }

    void setIdentifier(int i)
    {
        m_id = i;
    }
    int getIdentifier()
    {
        return m_id;
    }

    /*
        Diminui a m_frames remaining, sinalizando pro timer que um frame passou.

        caso o numero de quadro restantes seja maior que 0, a função subtrai e retorna true.

        Caso o numero de quadros restantes seja zero, o callback é executado e o seguinte ocorre:

        1 - se o timer for singleShot, a função retorna false.
        2 - se o timer não for singleShot, a função retorna true e m_frames_remaining recebe o valor de m_interval
    */
    bool decrementFrame();


    /*
        Cada entidade vai tem inteiro chamado event_state.

        A entidade só receberá os eventos da cena se a event_state de scene for igual a
        event_state da entidade.
    */
    inline int getEventState()
    {
        return m_event_state;
    }

    inline void setEventState(int e)
    {
        m_event_state = e;
    }

    void stopTimer()
    {
        setSingleShot(false);
        m_frames_remaining = -1;
    }

private:
    Interface* m_receptor;
    int m_id;
    bool m_single_shot;
    int m_frames_remaining;
    int m_interval;
    int m_event_state;
};

#endif // TIMER_H
