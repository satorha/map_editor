#include "TimerHandler.h"
#include <QDebug>


TimerHandler::TimerHandler()
{
    //ctor
    m_timers.reserve(64);
    next_timer_id = 0;
}

TimerHandler::~TimerHandler()
{
    //dtor
}

int TimerHandler::addTimer(Interface* receptor, int interval, bool singleshot, int event_state)
{
    Timer novo;
    novo.setReceptor(receptor);
    novo.setInterval(interval);
    novo.setSingleShot(singleshot);
    novo.setEventState(event_state);
    novo.setIdentifier(next_timer_id++);

    for(unsigned int i=0; i < m_timers.size(); i++)
    {
        if(!m_timers[i].isActive())
        {
            m_timers[i] = novo;
            return novo.getIdentifier();
        }
    }

    m_timers.push_back(novo);
    return novo.getIdentifier();
}

void TimerHandler::killTimer(int id)
{
    for(Timer& atual : m_timers)
    {
        if(atual.getIdentifier() == id)
        {
            atual.stopTimer();
            return;
        }
    }
    //LOG?
}

void TimerHandler::logic(int state_id)
{
    for(Timer& atual : m_timers)
    {
        if(atual.isActive() && (atual.getEventState() == state_id))
            atual.decrementFrame();
    }
}
