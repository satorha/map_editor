#include "Timer.h"
#include "Scene/Controller/Interface.h"

Timer::Timer()
{
    m_receptor = nullptr;
    m_id = -1;
    m_single_shot = false;
    m_frames_remaining = 0;
    m_interval = 0;
    m_event_state = 0;
}

Timer::~Timer()
{
    //dtor
}


bool Timer::decrementFrame()
{
    if(m_frames_remaining == 0)
    {
        m_receptor->timerCallback(this);

        if(m_single_shot)
        {
            m_frames_remaining = -1;
            return false;
        }


        m_frames_remaining = m_interval;
        return true;
    }

    m_frames_remaining--;

    return true;
}
