#ifndef TIMERHANDLER_H
#define TIMERHANDLER_H

#include <vector>
#include <algorithm>
#include "Timer.h"

class TimerHandler
{
public:
    TimerHandler();
    ~TimerHandler();

    int addTimer(Interface* receptor, int interval, bool singleshot, int event_state);
    void logic(int state_id);

    void killTimer(int id);
private:

    /*
        Pra cada timer, é armazenado tamém um bool que indica se o timer está ativo.
        Timers inativos serão substituidos na próxima chamda de addTimer().
    */
    std::vector<Timer> m_timers;
    int next_timer_id;
};

#endif // TIMERHANDLER_H
