#include "Scene.h"
#include "Sprite.h"
#include "Logger/Logger.h"
#define LOG() Logger::Log("Scene")
#include "ResourceManager/Filesystem.h"
#include "Controller/ControllerManager.h"

#include "Text/Text.h"
#define EDITOR_MODE

Scene::Scene()
{
    m_name = std::string("cena_padrao");
    m_event_state = 0;
    m_currently_reading = OBJ_SPRITE;
    m_first_execution = true;
    m_intersection_buffer.reserve(64);
}

Scene::~Scene()
{
    LOG() << "Desalocando cena (" << m_name << ").";
}

bool Scene::allReady()
{
    if(pendingObjects() > 0)
        return true;

    return false;
}

bool Scene::processItem(std::shared_ptr<Resource> res)
{
    Entity* atual = static_cast<Entity*>(res.get());

    std::cout << "processando item." << std::endl;
    if(!atual->loadDependencies())
    {
        LOG() << "processItem(): Erro ao carregar as texturas.";
        return false;
    }
    atual->setReady();

    //inicia o controlador
    if(atual->getController() != nullptr)
        atual->getController()->attach(atual, this);
    return true;
}

void Scene::deleteObject(Entity *obj)
{
    auto it = m_objects.find(obj->getName());

    if(it == m_objects.end())
    {
        LOG() << "Erro: deleteObject(): objeto não encontrado.";
        return;
    }

    sendEvent(obj, EVENT_DELETE);
    m_objects.erase(it);
}

int Scene::eraseUnused()
{
    LOG() << "eraseUnused() não está implementada.";
    //TODO: apagar objetos não utilizados
    return 0;
}

void Scene::draw()
{
for(auto& entry : m_objects)
    {
        //TODO: fazer logica pra desenhar apenas os que aparecem na tela.
        entry.second->draw();
    }
}
Entity* Scene::createObject(const char* name, int type, const char* controlador)
{
    std::string novo_nome;
    if(name == nullptr)
    {
 AGAIN:
        for(int i=0; i < 6; i++)
        {
            char atual = rand()%10 + '0';
            novo_nome += atual;
        }
        if(getObj(novo_nome.c_str()) != nullptr)
            goto AGAIN;

        name = novo_nome.c_str();
    }
    if(getObj(name) != nullptr)
    {
        LOG() << "já existe um objeto de nome \"" << name << "\".";
        return nullptr;
    }


    Entity* novo = Scene::allocator_exec(name, type);
    if(novo == nullptr)
    {
        LOG() << "createObject(): Erro na alocação de um objeto com nome \"" << name << "\" do tipo " << type << ".";
        return nullptr;
    }

    novo->setSecondName(name);
    novo->setReady();
    store_ptr(std::shared_ptr<Resource>((Resource*)novo));

    //inicia o controlador (se necessario)
    if(controlador != nullptr)
    {
        auto cont = ControllerManager::getController(controlador);
        attachController(novo, cont);

        sendEvent(novo, EVENT_BEGIN, nullptr);
    }



    LOG() << "createObject(): Novo objeto de nome \"" << name << "\".";
    return novo;
}

Entity* Scene::loadObject(const char* filename)
{
    return (Entity*)get(filename).get();
}

Entity* Scene::allocator_exec(const char* name, int type)
{
    const DirectoryTree* nodo = Filesystem::getNode(name);

    Entity* retorno = nullptr;
    switch(type)
    {
    case OBJ_SPRITE:
        retorno = new Sprite(nodo);
        break;
    case OBJ_TEXT:
        retorno = new Text(nodo);
        break;
    default:
        LOG() << "allocator_exec(): Tipo de objeto inválido.";
        break;
    }
    m_not_ready_yet.push_back(retorno);
    return retorno;
}

std::shared_ptr<Resource> Scene::alloc(const char* filename)
{
    return std::shared_ptr<Resource>(Scene::allocator_exec(filename, m_currently_reading));
}

void Scene::store_ptr(std::shared_ptr<Resource> coisa)
{
    m_objects[coisa->getName()] = std::static_pointer_cast<Entity>(coisa);
}

std::shared_ptr<Resource> Scene::find_ptr(const char* filename)
{
    auto it = m_objects.find(filename);
    if(it == m_objects.end())
        return nullptr;

    return it->second;
}

void Scene::sendEvent(Entity* obj, int type, void* data)
{
    Interface* atual = obj->getController();

    if(atual == nullptr)
        return; //não tem controlador associado, então não recebe nenhum evento

    if((type != EVENT_BEGIN) && (atual->getEventState() != m_event_state))
        return; //só recebe o evento se esses dois atributos forem iguais.
                //O evento begin sempre será executado, independente do valor de event_state
    switch(type)
    {
    case EVENT_MOVED:
        atual->eventMoved();
        break;

    case EVENT_FRAME:
        atual->onFrame();
        break;

    case EVENT_BEGIN:
        atual->eventBegin();
        break;
    case EVENT_KEYUP:
        atual->eventKeyup();
        break;

    case EVENT_MOUSEUP:
        atual->eventMouseButtonUp();
        break;

    case EVENT_MOUSEDOWN:
        atual->eventMouseButtonDown();
        break;
    case EVENT_KEYDOWN:
        atual->eventKeydown();
        break;

    case EVENT_CAMERAUPDATE:
        atual->eventCameraMoved();
        break;

    case EVENT_RESIZED:
        atual->eventResized();
        break;

    case EVENT_ANYMOUSEUP:
        atual->eventAnyMouseUp();
        break;
    case EVENT_DELETE:
        atual->eventDelete();
        break;
    //TODO: implementar os outros eventos.
    default:
        LOG() << "sendEvent(): tipo indefinido.";
        break;
    }
}


void Scene::rotateObj(Entity* obj, float rotacao)
{
    obj->m_area.sumRotation(rotacao);
    obj->areaModified();
    sendEvent(obj, EVENT_MOVED);
}

void Scene::translateObj(Entity* obj, float dx, float dy)
{
    obj->m_area.sum(dx, dy);
    obj->areaModified();
    sendEvent(obj, EVENT_MOVED);
}

void Scene::setObjPos(Entity* obj, float x, float y)
{
    obj->m_area.setX(x);
    obj->m_area.setY(y);
    obj->areaModified();
    sendEvent(obj, EVENT_MOVED);
}

void Scene::setObjSize(Entity* obj, float w, float h)
{
    obj->m_area.resize(w, h);
    obj->areaModified();
    sendEvent(obj, EVENT_RESIZED);
}

void Scene::setObjRotation(Entity* obj, float rotacao)
{
    obj->m_area.setRotation(rotacao);
    obj->areaModified();
    sendEvent(obj, EVENT_MOVED);
}

bool Scene::loadMap(const char* path)
{
    const DirectoryTree* nodo = Filesystem::getNode(path);

    LOG() << "carrengando mapa \"" << path << "\"";
    if(nodo == nullptr)
    {
        LOG() << "loadMap(): caminho inexistente. (" << path << ")";
        return false;
    }
    if(nodo->isFile())
    {
        LOG() << "loadMap(): caminho é um arquivo. (" << path << ")";
        return false;
    }

    /*
        Cada tipo de objeto tem um subdiretório próprio.

        Este for percorre todos eles, carregando os objetos.
    */

    std::vector<const DirectoryTree*> arquivos;
    for(int i=0; i < OBJ_TYPE_Q; i++)
    {
        arquivos.clear();
        const DirectoryTree* nodo_atual  = nodo->getNode(GetTypeString(i));

        if(nodo_atual == nullptr)
        {
            LOG() << "loadMap(): não existe diretório (" << GetTypeString(i) << ")";
            continue;
        }
        m_currently_reading = i;
        nodo_atual->listFiles(arquivos);

        LOG() << "loadMap(): " << arquivos.size() << " " << GetTypeString(i);

        for(const DirectoryTree* arquivo_atual : arquivos)
        {
            loadObject(arquivo_atual->getFullPath().c_str());
        }
    }

    return true;
}


#ifdef USAR_BOOST

#include <boost/filesystem.hpp>
#include <fstream>

bool Scene::saveMap(const char* path)
{
    LOG() << "Salvando mapa em " << path;

    //faz com q o caminho pra criar o mapa seja sempre terminado com '/'
    std::string mapa_caminho = path;
    if(mapa_caminho[mapa_caminho.size()-1] != '/')
        mapa_caminho.push_back('/');



    //começa criando a estrutura do diretório que representa o mapa
    //Cria-se uma pasta pra cada tipo de objeto
    //ou seja, uma pasta para sprite
    //outra pra Text etc
    for(int i=0; i < OBJ_TYPE_Q; i++)
    {
        std::string caminho_atual = (mapa_caminho + GetTypeString(i));


        if(!boost::filesystem::create_directories(caminho_atual.c_str()))
        {
            LOG() << "saveMap(): não foi possivel criar diretório \"" << caminho_atual << "\".";
            return false;
        }
    }

    //finalmente salva todos os objetos, criando um arquivo pra cada.
    for(auto& it : m_objects)
    {
        Entity* atual = it.second.get();

        std::string caminho_atual = (mapa_caminho + GetTypeString(atual->getType()) + "/" + atual->getName());
        LOG() << "Salvando arquivo \"" << caminho_atual << "\".";

        std::fstream novo_arquivo(caminho_atual.c_str(), std::fstream::out | std::fstream::binary);

        if(!novo_arquivo.is_open())
        {
            LOG() << "Erro ao salvar arquivo: não foi possível criar o arquivo \"" << caminho_atual << "\".";
            return false;
        }
        atual->saveToFile(novo_arquivo);

    }

    return true;

}

#endif // usar_boost

bool Scene::finishedLoading()
{
    std::list<Entity*>::iterator it = m_not_ready_yet.begin();

    while(it != m_not_ready_yet.end())
    {
        if(!(*it)->recursiveIsReady())
            return false;
        it = m_not_ready_yet.erase(it);
    }
    return true;
}

Entity* Scene::getObj(const char* name)
{
    auto it = m_objects.find(name);

    if(it == m_objects.end())
        return nullptr;

    return it->second.get();
}

void Scene::sendBeginEvent()
{
    auto it = m_objects.begin();

    while(it != m_objects.end())
    {
        sendEvent(it->second.get(), EVENT_BEGIN, nullptr);
        it++;
    }
}

void Scene::registerEvent(Interface* control, int tipo)
{
  m_events_queue.registerEvent(tipo, control);
}

void Scene::deregisterEvent(Interface* control, int tipo)
{
    m_events_queue.deregisterEvent(tipo, control);
}

#include "Input/Input.h"
void Scene::logic()
{
    /*
        Ao executar pela primeira vez, envia o evento begin() pra todos os elementos atualmente no mapa
    */
    if(m_first_execution)
    {
        sendBeginEvent();
        m_first_execution = false;
    }

    _current_input->processEvents(this);

    m_timer_handler.logic(getEventState());


}

int Scene::registerTimer(Interface* receptor, int interval, bool singleshot, int event_state)
{
    return m_timer_handler.addTimer(receptor, interval, singleshot, event_state);
}

void Scene::deregisterTimer(int id)
{
    LOG() << "Destruindo timer " << id;
    m_timer_handler.killTimer(id);
}

void Scene::notifyKeyup(int key)
{
    const std::vector<Interface*>& keyup_queue = m_events_queue.getQueue(EVENT_KEYUP);

    for(Interface* atual : keyup_queue)
    {
        sendEvent(atual->getEntity(), EVENT_KEYUP, nullptr);
    }
}

void Scene::notifyKeydown(int key)
{
    const std::vector<Interface*>& keydown_queue = m_events_queue.getQueue(EVENT_KEYDOWN);

    for(Interface* atual : keydown_queue)
    {
        sendEvent(atual->getEntity(), EVENT_KEYDOWN, nullptr);
    }
}

void Scene::notifyMouseButtonUp()
{
    const std::vector<Interface*>& anymouseup_queue = m_events_queue.getQueue(EVENT_ANYMOUSEUP);
    for(Interface* atual : anymouseup_queue)
    {
        sendEvent(atual->getEntity(), EVENT_ANYMOUSEUP, nullptr);
    }


    int x, y;
    _current_input->getMousePos(x, y);

    const std::vector<Entity*>& colisoes = getIntersections(Circle(x, y, 0));

    for(Entity* atual: colisoes)
    {
        sendEvent(atual, EVENT_MOUSEUP, nullptr);
    }




}

void Scene::notifyMouseButtonDown()
{
    int x, y;
    _current_input->getMousePos(x, y);

    const std::vector<Entity*>& colisoes = getIntersections(Circle(x, y, 0));

    for(Entity* atual: colisoes)
    {
        sendEvent(atual, EVENT_MOUSEDOWN, nullptr);
    }
}

#include "Util/CollisionTest.h"
const std::vector<Entity*>& Scene::getIntersections(const Circle& c)
{
    m_intersection_buffer.clear();
    for(auto it: m_objects)
    {
        Entity* atual = it.second.get();
        if(collisionTest(atual->getArea(), c))
        {
            m_intersection_buffer.push_back(atual);
        }

    }

    return m_intersection_buffer;
}

const std::vector<Entity*>& Scene::getIntersections(const Rect& r)
{
    m_intersection_buffer.clear();
    for(auto it: m_objects)
    {
        Entity* atual = it.second.get();
        if(intersection_test(atual, r))
        {
            m_intersection_buffer.push_back(atual);
        }

    }
    return m_intersection_buffer;
}
const std::vector<Entity*>& Scene::getIntersections(Entity& e)
{
    m_intersection_buffer.clear();
    for(auto it: m_objects)
    {
        Entity* atual = it.second.get();
        if(intersection_test(atual, &e).first != nullptr)
        {
            m_intersection_buffer.push_back(atual);
        }

    }
    return m_intersection_buffer;
}

void Scene::moveObjWithCamera(Entity* obj)
{
    obj->m_move_with_camera = true;
}

void Scene::notifyCameraMove()
{
    const std::vector<Interface*>& keyup_queue = m_events_queue.getQueue(EVENT_CAMERAUPDATE);

    for(Interface* atual : keyup_queue)
    {
        sendEvent(atual->getEntity(), EVENT_CAMERAUPDATE, nullptr);
    }
}

