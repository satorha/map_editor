#include "RegisteredEventsManager.h"

#include "Logger/Logger.h"
#define LOG() Logger::Log("RegisteredEventsManager")

RegisteredEventsManager::RegisteredEventsManager()
{
    //ctor
}

RegisteredEventsManager::~RegisteredEventsManager()
{
    //dtor
}


void RegisteredEventsManager::registerEvent(int type, Interface* control)
{
    std::vector<Interface*>& atual = *_getQueue(type);

    for(Interface* it : atual)
        if(it == control)
            return;
    _getQueue(type)->push_back(control);
}

void RegisteredEventsManager::deregisterEvent(int type, Interface* control)
{
    std::vector<Interface*>& atual = *_getQueue(type);

    for(unsigned int i=0; i < atual.size(); i++)
    {
        if(atual[i] == control)
        {
            atual.erase(atual.begin()+i);
            return;
        }
    }

    LOG() << "Erro ao desregistrar: Objeto não está na fila.";
}


std::vector<Interface*>* RegisteredEventsManager::_getQueue(int type)
{
    switch(type)
    {
    case EVENT_KEYDOWN:
        return &(m_queues[0]);

    case EVENT_KEYUP:
        return &(m_queues[1]);

    case EVENT_FRAME:
        return &(m_queues[2]);

    case EVENT_CAMERAUPDATE:
        return &(m_queues[3]);

    case EVENT_ANYMOUSEUP:
        return &(m_queues[4]);

    default:
        LOG() << "Erro: Evento de ID " << type << " não tem fila própria.";
        return nullptr;
    }
}
