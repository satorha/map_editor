#ifndef TEXT_H
#define TEXT_H

#include "Scene/Entity.h"
#include "Font.h"

class Text : public Entity
{
public:
    explicit Text(const DirectoryTree* p);

    virtual ~Text();

    //recebe o texto encodado em utf-8
    //esta função tentará carregar a fonte especificada como uma dependencia da entidade atual.
    void setText(const char* text);
    void setFont(const char* font);

    void setCharColor(int pos, uint8_t r, uint8_t g, uint8_t b);

    //pega o tamanho do texto antes de ser redimensionado pra caber na área definida
    int getNormalWidth();
    int getNormalHeight();

    virtual const std::vector<hitbox_t>& getHitboxes();

    virtual void draw();
    virtual bool loadDependencies();
    virtual OBJ_TYPE getType();


    Rect getTextArea();
    void drawTextArea();
    virtual void areaModified();

    void resetSize();


    virtual bool saveToFile(std::ostream& out);
    virtual bool loadFromData(MemoryFile* f);

    virtual void dependenciesReady();

    std::string getOriginalText()
    {
        return m_utf8_original;
    }
    std::string getFontFilename()
    {
        if(m_fonte == nullptr)
            return "";
        return m_fonte->getFullPath();
    }

private:
    float maior_largura;
    float maior_altura;
    //depois que a fonte for carregada, esta função será chamada para enfim inicializar o texto.
    void updateText();

    Rect m_text_area;
    std::vector<hitbox_t> m_hitbox;

    void fitToArea();
    void loadFont();


    std::shared_ptr<Font> m_fonte;
    std::vector<glyph> m_string;

    std::string m_utf8_original;
    std::string m_font_path;

};

#endif // TEXT_H
