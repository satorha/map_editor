#ifndef FONT_H
#define FONT_H

#include "ResourceManager/Resource.h"
#include "Renderer/Texture/Texture.h"
#include <unordered_map>

#include <freetype2/ft2build.h>
#include <freetype2/freetype.h>
#include <freetype2/ftglyph.h>
#include <freetype2/ftoutln.h>
#include <freetype2/fttrigon.h>

struct glyph
{
    std::shared_ptr<Texture> m_bitmap;
    int w;
    int h;
    int advance_x;
    int bearing_y;
    int bearing_x;
    Rect currentpos;
    float r;
    float g;
    float b;
};

class Font : public Resource
{
public:
    explicit Font(const DirectoryTree* p, int size);

    virtual void onDataAvailable(std::vector<char>& data);

    virtual ~Font();

    int getSize()
    {
        return m_size;
    }

    glyph getGlyph(uint32_t c);

    bool glyphLoaded(uint32_t c);

private:
    friend class FontManager;

    FT_Face m_face_atual;

    //o arquivo .ttf fica armazenado aqui até ser interpretado
    std::vector<char> m_ttf_buffer;

    int m_size;



    void addGlyph(uint32_t c, glyph& g);

    std::unordered_map<uint32_t, glyph> m_dicionario; //cada caractere é representado em utf32


};

#endif // FONT_H
