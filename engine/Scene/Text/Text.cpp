#include "Text.h"

#include "Logger/Logger.h"
#define LOG() Logger::Log("Text")

#include "Renderer/Renderer.h"
#include "FontManager.h"
#include "Util/Serialization.h"


void preenche_int(uint32_t& integer, uint8_t current_byte, uint8_t current_byte_pos)
{
    while(current_byte_pos != 0)
    {
        integer <<= 1;
        if(current_byte & current_byte_pos)
        {
            integer |= 1;
        }

        current_byte_pos >>= 1;
    }
}

/*
    Pega um ponteiro pra o inicio de uma string utf8 e armazena o proximo caractere em resultado.

    retorna um ponteiro pra posição do próximo caractere.
*/
const uint8_t* utf8_to_32(const uint8_t* utf8, uint32_t& resultado)
{
    resultado=0;

    uint8_t bit_inicial = *(utf8++);

    uint8_t pos_atual=0b10000000;

    int quant=0;
    //conta quantos 1 no comeco
    while(bit_inicial & pos_atual)
    {
        pos_atual >>= 1;
        quant++;
    }
    pos_atual >>= 1; //pula um zero

    //copia os bits restantes
    preenche_int(resultado, bit_inicial, pos_atual);

    if(quant >= 2) preenche_int(resultado, *(utf8++), 0b10000000>>2);
    if(quant == 3) preenche_int(resultado, *(utf8++), 0b10000000>>2);

    return utf8;
}


Text::Text(const DirectoryTree* p) :
    Entity(p)

{
    //ctor
    m_hitbox.resize(1);
    maior_largura = 0;
    maior_altura = 0;
}

Text::~Text()
{
    //dtor
}

int Text::getNormalWidth()
{
    return 0;
}

int Text::getNormalHeight()
{
    return 0;
}

inline float abs(float c)
{
    if(c < 0) return -c;
    return c;
}

void Text::resetSize()
{
    maior_altura = 0;
    maior_largura = 0;

    std::string copia = m_utf8_original;
    setText(copia.c_str());
}

void Text::updateText()
{
    if(m_fonte == nullptr)
        return;
    const uint8_t* text = (const uint8_t*)&m_utf8_original[0];

    m_string.clear();
    uint32_t char_atual;
    text = utf8_to_32(text, char_atual);

    int current_x=0;
    while(char_atual != 0)
    {
        fontManager.addGlyph(char_atual, m_fonte.get());
        m_string.push_back(m_fonte->getGlyph(char_atual));

        glyph& caractere = m_string.back();
        caractere.currentpos = Rect(current_x + caractere.w/2 + caractere.bearing_x, -(caractere.bearing_y - caractere.h/2), caractere.w, caractere.h);

        text = utf8_to_32(text, char_atual);
        current_x += caractere.advance_x;
    }

    const float largura_total = std::max(m_string.back().currentpos.getX() + m_string.back().currentpos.getW(), maior_largura);
    maior_largura = largura_total;

    //calcula a altura do texto
    float maior_desvio=0;
    for(glyph& atual : m_string)
    {
        float desvio = abs(atual.currentpos.getY()) + atual.currentpos.getW()/2;
        maior_desvio = std::max(maior_desvio, desvio);
    }
    maior_altura = std::max(maior_altura, maior_desvio*2);

    //define a area do texto
    m_text_area = Rect(0, 0, largura_total, maior_altura);


    //centraliza o texto na hitbox
    for(glyph& atual : m_string)
    {
        atual.currentpos.sum(-largura_total/2, m_fonte->getSize()/2);
    }


    fitToArea();

}

void Text::setText(const char* text)
{
    m_utf8_original = (char*)text;

    if(recursiveIsReady()) updateText();
}

void Text::setFont(const char* path)
{
    m_font_path = path;
    loadFont();
}

void Text::setCharColor(int pos, uint8_t r, uint8_t g, uint8_t b)
{
    glyph& atual = m_string[pos];
    atual.r = r/255.0;
    atual.g = g/255.0;
    atual.b = b/255.0;
}


Rect Text::getTextArea()
{
    return Rect(getX(), getY(), m_text_area.getW(), m_text_area.getH());
}

void Text::drawTextArea()
{
    Renderer::drawQuad(getTextArea(), nullptr, 0.2, 0);
}

void Text::fitToArea()
{
    float razao = std::min(getArea().getW()/getTextArea().getW(), getArea().getH()/getTextArea().getH());


    m_text_area.multiply(razao, razao);
    for(glyph& atual : m_string)
    {
        atual.currentpos.multiply(razao, razao);
    }

}

const std::vector<Entity::hitbox_t>& Text::getHitboxes()
{
    m_hitbox[0].area = getTextArea();
    return m_hitbox;
}
void Text::draw()
{
    if(m_fonte == nullptr)
    {
        Renderer::drawQuad(getArea(), nullptr, getAlpha(), getLayer());
        return;
    }
    if(!m_fonte->recursiveIsReady())
        return;

    drawTextArea();
    for(glyph& caractere : m_string)
    {
        Rect copia  = caractere.currentpos;
        copia.sum(getX(), getY());
        Renderer::drawQuad(copia, caractere.m_bitmap.get(), getAlpha(), getLayer(), caractere.r, caractere.g, caractere.b);
    }
}

bool Text::loadDependencies()
{
    loadFont();
    return true;
}

OBJ_TYPE Text::getType()
{
    return OBJ_TEXT;
}

void Text::areaModified()
{
    fitToArea();
}

void Text::loadFont()
{
    m_fonte = std::static_pointer_cast<Font>(fontManager.get(m_font_path.c_str()));
    addDependency(m_fonte);
}

bool Text::saveToFile(std::ostream& out)
{
    Entity::saveToFile(out);

    write_var_to_file(m_utf8_original, out);
    write_var_to_file(m_fonte->getFullPath(), out);
    return true;
}

bool Text::loadFromData(MemoryFile* m)
{
    Entity::loadFromData(m);

    read_var_from_bin(m_utf8_original, m);
    read_var_from_bin(m_font_path, m);
    return true;
}

void Text::dependenciesReady()
{
    updateText();
}
