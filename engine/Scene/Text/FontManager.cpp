#include "FontManager.h"
#include "Util/Panic.h"
#include "Logger/Logger.h"
#include "ResourceManager/Filesystem.h"
#include "Renderer/Texture/TextureManager.h"

#define LOG() Logger::Log("FontManager")

FontManager fontManager;

FT_Library _library = nullptr;

FontManager::FontManager()
{
    if(_library != nullptr)
    {
        Panic("Alocando mais de um fontmanager.");
    }
    if (FT_Init_FreeType( &_library ))
    {
        Panic("Erro ao iniciar biblioteca de fontes.\n");
    }
}

FontManager::~FontManager()
{
    LOG() << "Destruindo fontmanager.";

    if(_library != nullptr)
    {
        FT_Done_FreeType(_library);
    }
}

bool FontManager::processItem(std::shared_ptr<Resource> res)
{
    std::shared_ptr<Font> atual = std::static_pointer_cast<Font>(res);

    LOG() << "Processando fonte \"" << atual->getFullPath() << "\" tam: " << atual->getSize();

    initFont(atual.get());


    return true;

}

void FontManager::initFont(Font* f)
{
    if (FT_New_Memory_Face(_library, (FT_Byte*)&f->m_ttf_buffer[0], f->m_ttf_buffer.size(), 0, &f->m_face_atual))
    {
        LOG() << "Erro ao abrir fonte \"" << f->getFullPath() << "\".";
        Panic("");
        return;
    }

    FT_Select_Charmap(f->m_face_atual, ft_encoding_unicode);


    FT_Set_Char_Size( f->m_face_atual, f->m_size << 6, f->m_size << 6, 96, 96);

    for(uint32_t i=0; i < 255; i++)
    {
        addGlyph(i, f);
    }

    LOG() << "Carregada a fonte \"" << f->getFullPath() << "\".";
}

void FontManager::addGlyph(uint32_t c, Font* f)
{
    if(f->glyphLoaded(c))
        return; //nao carrega o mesmo caractere 2 vezes.
    // Load The Glyph For Our Character.
    if(FT_Load_Glyph( f->m_face_atual, FT_Get_Char_Index( f->m_face_atual, c), FT_LOAD_DEFAULT ))
    {
        LOG() << "FT_Load_Glyph falhou.";
        return;
    }

    // Move The Face's Glyph Into A Glyph Object.
    FT_Glyph _glyph;
    if(FT_Get_Glyph( f->m_face_atual->glyph, &_glyph ))
    {
        LOG() << "FT_Get_Glyph falhou.";
        return;
    }

    // Convert The Glyph To A Bitmap.
    FT_Glyph_To_Bitmap( &_glyph, ft_render_mode_normal, 0, 1 );
    FT_BitmapGlyph bitmap_glyph = (FT_BitmapGlyph)_glyph;

    // This Reference Will Make Accessing The Bitmap Easier.
    FT_Bitmap& bitmap=bitmap_glyph->bitmap;

    std::vector<uint8_t> expanded_data(4*bitmap.width*bitmap.rows);

    for(unsigned int i=0; i < expanded_data.size(); i++)
        expanded_data[i] = 0;

    int width = bitmap.width;
    int height = bitmap.rows;
    for(int j=0; j < height; j++)
    {
        for(int i=0; i < width; i++)
        {
            expanded_data[4*(i+j*width)] = 255;
            expanded_data[4*(i+j*width)+1] = 255;
            expanded_data[4*(i+j*width)+2] = 255;
            expanded_data[4*(i+j*width)+3] = bitmap.buffer[(i + width * j)];
        }
    }

    std::shared_ptr<Texture> tex = texManager.createTexture(&expanded_data[0], width, height);

    glyph novo;
    novo.w = width;
    novo.h = height;
    novo.advance_x = (f->m_face_atual->glyph->metrics.horiAdvance>>6) - (f->m_face_atual->glyph->metrics.horiBearingX>>6);
    novo.bearing_y = f->m_face_atual->glyph->metrics.horiBearingY>>6;
    novo.m_bitmap = tex;
    novo.bearing_x = f->m_face_atual->glyph->metrics.horiBearingX>>6;
    novo.r = novo.g = novo.b = 1;

    f->addGlyph(c, novo);

    FT_Done_Glyph( _glyph );
}

FONT_ENTRY fromStr(std::string filename)
{
    FONT_ENTRY retorno;
    retorno.second = 100;

    int pos = filename.find_last_of(':');
    retorno.first = filename.substr(0, pos);
    if(pos != -1) retorno.second = std::stoi(filename.substr(pos+1));

    return retorno;
}

std::shared_ptr<Resource> FontManager::alloc(const char* filename)
{
    //no caso de fontes, o filename estará codificado assim
    //lugar/coisa.ttf:56
    //em q no lugar de 56, se coloca o tamanho da fonte.
   FONT_ENTRY dados = fromStr(filename);

    LOG() << "Alocando \"" << dados.first << "\" de tamanho " << dados.second;

    auto path = Filesystem::getNode(dados.first.c_str());

    return std::shared_ptr<Resource>(new Font(path, dados.second));
}

int FontManager::eraseUnused()
{
    LOG() << "EraseUnused não implementada.";
    return 0;
}

void FontManager::store_ptr(std::shared_ptr<Resource> res)
{
    std::shared_ptr<Font> fonte = std::static_pointer_cast<Font>(res);
    FONT_ENTRY nova(res->getFullPath(), fonte->getSize());

    auto it = m_fonts.find(nova);
    if(it != m_fonts.end())
    {
        LOG() << "Erro: ja existe uma fonte com esses parametros carregada.";
        return;
    }


    m_fonts[nova] = fonte;
}

std::shared_ptr<Resource> FontManager::find_ptr(const char* filename)
{
    FONT_ENTRY dados = fromStr(filename);

    auto it = m_fonts.find(dados);
    if(it == m_fonts.end())
        return nullptr;


    return it->second;
}
