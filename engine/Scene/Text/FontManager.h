#ifndef FONTMANAGER_H
#define FONTMANAGER_H

#include <ResourceManager/TypeManager.h>
#include "Font.h"
#include <map>

typedef std::pair<std::string, int> FONT_ENTRY;

class FontManager : public TypeManager
{
public:
    FontManager();
    virtual ~FontManager();

    virtual bool processItem(std::shared_ptr<Resource> res);
    virtual int eraseUnused();
    virtual std::shared_ptr<Resource> alloc(const char* filename);
    virtual void store_ptr(std::shared_ptr<Resource> res);
    virtual std::shared_ptr<Resource> find_ptr(const char* filename);

    void addGlyph(uint32_t c, Font* f);

protected:
private:

    void initFont(Font* f);

    std::map<FONT_ENTRY, std::shared_ptr<Font> > m_fonts;

};

extern FontManager fontManager;
#endif // FONTMANAGER_H
