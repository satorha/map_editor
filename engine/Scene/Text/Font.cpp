#include "Font.h"


#include "Logger/Logger.h"
#define LOG() Logger::Log("Text")


Font::Font(const DirectoryTree* p, int size) :
    Resource(p)
{
    m_size = size;
}

Font::~Font()
{

}


void Font::onDataAvailable(std::vector<char>& data)
{
    m_ttf_buffer = std::move(data);
}

void Font::addGlyph(uint32_t c, glyph& g)
{
    m_dicionario[c] = g;
    addDependency(g.m_bitmap);
}

bool Font::glyphLoaded(uint32_t c)
{
    auto it = m_dicionario.find(c);
    return (it != m_dicionario.end());
}


glyph Font::getGlyph(uint32_t c)
{
    auto it = m_dicionario.find(c);

    if(it == m_dicionario.end())
    {
        LOG() << "Caractere não carregado pela fonte.";
        return getGlyph(' ');
    }

    return it->second;
}
