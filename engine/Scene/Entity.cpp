#include "Entity.h"
#include "Util/Serialization.h"
#include "Controller/ControllerManager.h"
#include "Util/CollisionTest.h"
#include "Renderer/Renderer.h"

#include "Logger/Logger.h"
#define LOG() Logger::Log("Scene")

Entity::Entity(const DirectoryTree* filename) :
    Resource(filename)
{
    //ctor
    m_area = Rect(0, 0, 256, 256);
    alpha = 1;
    layer = 1;
    m_controller = nullptr;
    m_move_with_camera = false;
}

Entity::~Entity()
{
    //dtor
}

bool Entity::saveToFile(std::ostream& out)
{
    write_var_to_file(layer, out);
    write_var_to_file(alpha, out);
    write_var_to_file(m_area, out);
    write_var_to_file(getControllerName(), out);

    return true;
}



bool Entity::loadFromData(MemoryFile* f)
{
    read_var_from_bin(layer, f);
    read_var_from_bin(alpha, f);
    read_var_from_bin(m_area, f);

    std::string controller_name;
    read_var_from_bin(controller_name, f);

    if(!controller_name.empty())
    {
        m_controller = ControllerManager::getController(controller_name.c_str());

        if(m_controller == nullptr)
        {
            LOG() << "Controlador \"" << controller_name << "\" não encontrado.\n";
            return false;
        }
    }



    return true;
}

void Entity::onDataAvailable(std::vector<char>& data)
{
    MemoryFile arquivo;
    arquivo.setData(data);
    loadFromData(&arquivo);
}

void Entity::drawHitboxes()
{
    const std::vector<hitbox_t>& hitboxes = getHitboxes();

    for(const hitbox_t& hitbox : hitboxes)
    {
        Renderer::drawQuad(hitbox.area, nullptr, 0.6, 0);
    }
}

const char* GetTypeString(int tipo)
{
    switch(tipo)
    {
    case OBJ_SPRITE:
        return ("Sprite");

    case OBJ_TEXT:
        return ("Text");

    default:
        return ("Error");
    }
}


std::pair<const Entity::hitbox_t*, const Entity::hitbox_t*> intersection_test(Entity* e1, Entity* e2)
{
    const std::vector<Entity::hitbox_t>& hitboxes_1 = e1->getHitboxes();
    const std::vector<Entity::hitbox_t>& hitboxes_2 = e2->getHitboxes();

    for(unsigned int i=0; i < hitboxes_1.size(); i++)
    {
        for(unsigned int a=0; a < hitboxes_2.size(); a++)
        {
            if(collisionTest(hitboxes_1[i].area, hitboxes_2[a].area))
                return std::pair<const Entity::hitbox_t*, const Entity::hitbox_t*>(&hitboxes_1[i], &hitboxes_2[i]);
        }
    }
    return std::pair<const Entity::hitbox_t*, const Entity::hitbox_t*>(nullptr, nullptr);
}

const Entity::hitbox_t* intersection_test(Entity* e1, const Rect& r)
{
    const std::vector<Entity::hitbox_t>& hitboxes_1 = e1->getHitboxes();

    for(unsigned int i=0; i < hitboxes_1.size(); i++)
    {
        if(collisionTest(hitboxes_1[i].area, r))
            return &hitboxes_1[i];

    }
    return nullptr;
}

const Entity::hitbox_t* intersection_test(Entity* e1, const Circle& c)
{
    const std::vector<Entity::hitbox_t>& hitboxes_1 = e1->getHitboxes();

    for(unsigned int i=0; i < hitboxes_1.size(); i++)
    {
        if(collisionTest(hitboxes_1[i].area, c))
            return &hitboxes_1[i];

    }
    return nullptr;
}
