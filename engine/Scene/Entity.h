#ifndef ENTITY_H
#define ENTITY_H

#include "ResourceManager/Resource.h"
#include "Util/Rect.h"
#include <vector>
#include "ResourceManager/MemoryFile.h"
#include <ostream>
#include "Controller/Interface.h"
#include <memory>


enum OBJ_TYPE
{
    OBJ_SPRITE,
    OBJ_TEXT,
    OBJ_TYPE_Q
};



class Entity : public Resource
{
public:
    // esta classe nunca deve ser instanciada diretamente
    // a classe Scene que cuida de classes deste tipo
    //neste construtor devem ser adicionadas as dependências (ex.: texturas)
    Entity(const DirectoryTree* p);
    virtual ~Entity();

    struct hitbox_t
    {
        Rect area;
        std::string name;
    };

    const Rect& getArea()
    {
        return m_area;
    }

    inline int getX()
    {
        return m_area.getX();
    }
    inline int getY()
    {
        return m_area.getY();
    }
    inline int getW()
    {
        return m_area.getW();
    }
    inline int getH()
    {
        return m_area.getH();
    }
    inline int getRotation()
    {
        return m_area.getRotation();
    }
    inline float getAlpha()
    {
        return alpha;
    }
    inline int getLayer()
    {
        return layer;
    }

    virtual const std::vector<hitbox_t>& getHitboxes() = 0;

    virtual void draw()=0;

    /*
        esta função deve ser usada apenas internamente pela engine.
        Para responder a algum movimento, é melhor usar o evento onmove.

        Este callback é chamado quando a área ocupada pela entidade for modificada de alguma forma. (movida ou redimensionada)
    */
    virtual void areaModified(){};

    void drawHitboxes();

    /*
        Se necessário, a entidade pode carregar outros recursos.

        Ex. um Sprite precisa carregar pelo menos uma textura.
            um Texto precisa carregar uma fonte.

        Esta função pode simplismente chamar o gerenciador de cada um dos recursos necessários pelo método get(),
        pois como ela é chamada pela thread principal, nenhum problema irá ocorrer.
    */
    virtual bool loadDependencies()=0;

    /*
        Retorna o tipo do objeto.
        Sempre retorna um valor do enum OBJ_TYPE
    */
    virtual OBJ_TYPE getType()=0;



    /*
        Nesta função deve ficar a lógica que decodifica o objeto a partir dos dados.

        A leitura de um objeto a partir de um arquivo tem 2 passos.

        No primeiro (executado por esta função), ele interpreta os dados (posição, tamanho etc)
        e escreve na estrutura.

        O segundo passo é executado pela classe Scene, que vai colocar o objeto no lugar (quadtree)
        e mudar o status deste resource pra ready.

    */
    virtual void onDataAvailable(std::vector<char>& data);




    /*
        Escreve o estado da entidade na stream.

        Retorna false em caso de erro.
    */
    virtual bool saveToFile(std::ostream& out);

    /*
        Muda o estado da entidade para o estado salvo em binário.

        retorna false em caso de erro.
    */
    virtual bool loadFromData(MemoryFile* f);

    Interface* getController()
    {
        return m_controller.get();
    }
    std::string getControllerName()
    {
        if(m_controller == nullptr)
            return "";

        return m_controller->getName();
    }


    /*
        Se for true, o objeto está "colado" na "câmera".
        Ou seja, as transformações causadas por alterações na câmera não afetam este objeto.
    */
    bool moveWithCamera()
    {
        return m_move_with_camera;
    }


    void setLayer(uint8_t n)
    {
        layer = n;
    }

    void setAlpha(float f)
    {
        alpha = f;
    }

protected:




private:
    friend class Scene;

    uint8_t layer; // a camada só pode ser alterada indiretamente pela classe scene
    float alpha;
    Rect m_area;
    std::unique_ptr<Interface> m_controller;

    bool m_move_with_camera;
};


/*
    Testa a colisão entre uma entidade e alguma outra coisa.

    Retorna um ponteiro a hitbox em que a interseção foi detectada.
*/


std::pair<const Entity::hitbox_t*, const Entity::hitbox_t*> intersection_test(Entity* e1, Entity* e2);
const Entity::hitbox_t* intersection_test(Entity* e1, const Rect& r);
const Entity::hitbox_t* intersection_test(Entity* e1, const Circle& c);

const char* GetTypeString(int tipo);
#endif // ENTITY_H
