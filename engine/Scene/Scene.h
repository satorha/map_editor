#ifndef SCENE_H
#define SCENE_H

#include <unordered_map>
#include <string>
#include <list>
#include "Entity.h"
#include "Util/Transformation.h"
#include "RegisteredEventsManager.h"
#include "Timer/TimerHandler.h"


#define USAR_BOOST



#include "ResourceManager/TypeManager.h"


/*
    OBS: cada novo tipo de objeto criado deve ser adicionado na função Scene::allocator_exec();

*/
class Scene : private TypeManager
{
public:
    Scene();
    virtual ~Scene();

    /*
        Retorna true se todos os objetos estiverem prontos pra uso.
        Isto é, não estão esperando dados.

        Podemos considerar que uma fase só termina de carregar quando allReady() retornar true.
    */
    bool allReady();

    void logic();

    /*
        Desenha os objetos visíveis
    */
    void draw();

    /*
        Cria um novo objeto e retorna o ponteiro.

        Esta função usa o alocador passando type como argumento.

        Se já existir um objeto com o nome especificado, retornará null e mostrará uma mensagem de erro no log.

        O evento BEGIN será enviado ao controlador (caso especifique um).
    */
    Entity* createObject(const char* name, int type, const char* controlador);

    /*
        Carrega um objeto a partir de um arquivo.

        Essa função chama a alloc() para criar o objeto,
        portanto, filename deve ser um objeto do mesmo tipo especificado em m_currently_reading
    */
    Entity* loadObject(const char* filename);

    /*
        Deleta o objeto que possuir o nome especificado.
    */
    void deleteObject(Entity* obj);

    /*
        Aplica uma transformação a um objeto.
        A transformação sempre é aplicada em relação ao centro atual do objeto.
    */
    void rotateObj(Entity* obj, float angle);
    void translateObj(Entity* obj, float dx, float dy);
    void setObjSize(Entity* obj, float w, float h);
    void setObjPos(Entity* obj, float x, float y);
    void setObjRotation(Entity* obj, float angle);

    /*
        "Trava" a posição da entidade na câmera.

        Depois desta chamada, as posições X e  Y do objeto sempre vão ser relativas ao centro da tela
        em vez de ser ao início da cena.
    */
    void moveObjWithCamera(Entity* obj);

    bool loadMap(const char* path);

    bool saveMap(const char* path);

    /*
        Retorna true se não houver nenhum objeto esperando ser carregado.
    */
    bool finishedLoading();

    Entity* getObj(const char* name);



    /*
        Apenas entidades que tiverem event_state numericamente igual a m_event_state receberão eventos.

        Assim, para pausar o jogo, basta mudar m_event_state pra qualquer outro valor.

        Por padrão, m_event_state é iniciado em zero.
    */
    int getEventState()
    {
        return m_event_state;
    }
    void setEventState(int n)
    {
        m_event_state = n;
    }

    /*
        Bota um controlador pra controlar um certo objeto
    */
    void attachController(Entity* obj, std::unique_ptr<Interface>& control)
    {
        obj->m_controller = std::move(control);
        obj->m_controller->attach(obj, this);
    }

    //estas duas funções são usadas diretamente pelo controlador
    void registerEvent(Interface* control, int tipo);
    void deregisterEvent(Interface* control, int tipo);

    int registerTimer(Interface* receptor, int interval, bool singleshot, int event_state);
    void deregisterTimer(int id);


    /*
        Essas funções são chamadas pelo gerenciador de entrada.

        Servem para emitir os eventos para os controladores.

    */
    /*
       Essas funções mandam o evento pro objeto que estiver abaixo do ponteiro do mouse
    */
    void notifyMouseButtonDown();

    //obs: o evento anyMouseButtonUp sempre é enviado antes do mousebuttonup de cada entidade
    void notifyMouseButtonUp();

    /*
        Já essas, enviam pros objetos que tiverem registrado o recebimento do respectivo evento
    */
    void notifyKeyup(int key);
    void notifyKeydown(int key);
    void notifyMouseMove();
    void notifyCameraMove();


    /*
        Esses métodos são uma forma rápida de retornar a lista de objetos que estão em uma certa área.

        Esta área pode ser um círculo, um retangulo ou mesmo outro objeto.
    */
    //TODO: otimizar estas funções com alguma estrutura de particionamento da cena (quadtrees?)
    const std::vector<Entity*>& getIntersections(const Circle& c);
    const std::vector<Entity*>& getIntersections(const Rect& r);
    const std::vector<Entity*>& getIntersections(Entity& e);

    int completedObjects()
    {
        return TypeManager::completedObjects();
    }

protected:
    bool processItem(std::shared_ptr<Resource> res);
    int eraseUnused();

    /*
        Aloca um novo objeto.
        Ele será do tipo determinado pelo membro "m_currently_reading"
    */
    std::shared_ptr<Resource> alloc(const char* filename);

    void store_ptr(std::shared_ptr<Resource> res);

    std::shared_ptr<Resource> find_ptr(const char* filename);

private:
    void sendEvent(Entity* obj, int type, void* data=nullptr);
    void updateCameraObjects();

    std::string m_name;
    std::unordered_map<std::string, std::shared_ptr<Entity> > m_objects;
    int m_event_state;

    /*
        Apenas aloca memória para um novo objeto do tipo especificado.
    */
    Entity* allocator_exec(const char* filename, int type);

    /*
        Armazena o tipo que o próximo objeto a ser alocado terá.

        Ex: se m_currently_reading for OBJ_SPRITE, a função
    */
    int m_currently_reading;

    std::list<Entity*> m_not_ready_yet;

    //esta flag é false caso logic() tenha sido chamada ao menos uma vez.
    bool m_first_execution;

    /*
        manda o evento begin pra todos as entidades na cena
    */
    void sendBeginEvent();


    /*
        Esse membro gerencia os eventos que precisam ser registrados, como o frame, keydown, keyup etc.
    */
    RegisteredEventsManager m_events_queue;


    /*
        Controla os timers da cena
    */
    TimerHandler m_timer_handler;



    std::vector<Entity*> m_intersection_buffer;
};

#endif // SCENE_H
