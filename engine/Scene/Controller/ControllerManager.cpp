#include "ControllerManager.h"
#include <unordered_map>
#include <string>
#include <memory>
#include "Logger/Logger.h"
#include <string>
#include <iostream>
#include "Util/Panic.h"

#define LOG() Logger::Log("Scene")


std::unordered_map<std::string, INTERFACE_ALLOCATOR_CALLBACK> _tipos_registrados;

ControllerManager::ControllerManager()
{

}

void ControllerManager::registerController(INTERFACE_ALLOCATOR_CALLBACK callback)
{
    std::unique_ptr<Interface> ptr(callback()); //aloca uma interface temporária para obter o nome
    std::string name = ptr->getName();
    _tipos_registrados[name] = callback;
}


std::unique_ptr<Interface> ControllerManager::getController(const char* name)
{
    auto it = _tipos_registrados.find(name);

    if(it == _tipos_registrados.end())
    {
        LOG() << "Controlador \"" << name  << "\" não encontrado.";
        Panic("Tentativa de alocar um controlador inexistente.");
        return nullptr;
    }
    INTERFACE_ALLOCATOR_CALLBACK callback = it->second;
    return std::unique_ptr<Interface>(callback());
}

