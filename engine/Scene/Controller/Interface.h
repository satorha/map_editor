#ifndef INTERFACE_H
#define INTERFACE_H

#include "Scene/Timer/Timer.h"
#include <QDebug>

class Scene;

class Entity;

class Timer;

/*
    Este objeto representa um controlador.

    Cada interface dessa controla uma entidade dentro da cena, podendo potencialmente interagir com outras

*/
class Interface
{
public:
    explicit Interface();
    virtual ~Interface();

    void attach(Entity* obj, Scene* cena)
    {
        m_attached_obj = obj;
        m_current_scene = cena;
    }

    virtual const char* getName()=0;

    /*
        Esses são callbacks para eventos que ocorram com este objeto.
    */
    //os seguintes callbacks só são enviados quando o objeto atual estiver registrado
    virtual void onFrame() {}; //ok
    virtual void eventKeydown() {}; //ok
    virtual void eventKeyup() {}; //ok
    virtual void eventAnyMouseUp(){};


    //os seguintes callbacks sempre são chamados, não necessitando registro nenhum
    virtual void eventBegin() {}; //ok
    virtual void eventMouseButtonDown() {}; //ok
    virtual void eventMouseButtonUp() {}; //ok
    virtual void eventMoved() {};
    virtual void eventCameraMoved(){};
    virtual void eventResized(){};
    virtual void eventDelete()
    {
        qDebug() << "deletado";
    };

    virtual void timerCallback(Timer* h){};
    /////// fim da lista de eventos ////////////////////////////////////////////////////

    inline Entity* getEntity()
    {
        return m_attached_obj;
    }

    inline Scene* getScene()
    {
        return m_current_scene;
    }


    /*
        Cada entidade vai tem inteiro chamado event_state.

        A entidade só receberá os eventos da cena se a event_state de scene for igual a
        event_state da entidade.
    */
    inline int getEventState()
    {
        return m_event_state;
    }

    inline void setEventState(int e)
    {
        m_event_state = e;
    }

private:
    Entity* m_attached_obj;
    Scene* m_current_scene;
    int m_event_state;

};

#endif // INTERFACE_H
