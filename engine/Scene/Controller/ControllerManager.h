#ifndef CONTROLLERMANAGER_H
#define CONTROLLERMANAGER_H

#include "Interface.h"
#include <memory>

typedef Interface* (*INTERFACE_ALLOCATOR_CALLBACK)();

class ControllerManager
{
public:

    /*
        Registra um novo tipo de controlador.

        A função especificada deve alocar a interface e retornar o ponteiro.
        Não é necessário se preocupar em deletar este objeto mais tarde, pois a entidade fará isso.
    */
    static void registerController(INTERFACE_ALLOCATOR_CALLBACK callback);

    /*
        Aloca um controlador do nome especificado.
    */
    static std::unique_ptr<Interface> getController(const char* name);



protected:
    ControllerManager();
};

#endif // CONTROLLERMANAGER_H

