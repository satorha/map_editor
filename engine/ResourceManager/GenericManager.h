#ifndef GENERICMANAGER_H
#define GENERICMANAGER_H

#include "MultithreadDataStructure/MultithreadQueue.h"

#include <map>
#include <vector>
#include <iostream>

#include "ResourceReference.h"

template<typename T>
class GenericManager
{
protected:
    //MultithreadQueue< Resource* > _to_decode;

    int current_id;
    std::map<std::string, int> m_dictionary; //traduz as strings pra certo id

    struct cell
    {
        cell()
        {
            count = 0;
        }
        T obj;
        int count;
    };
    std::vector<cell> m_storage; //armazena de fato todas as coisas

public:
    GenericManager()
    {
        current_id = 0;
    }
    virtual ~GenericManager()
    {

    }

    ResourceRef<T> alocar()
    {
        m_storage.push_back(cell());
        return ResourceRef<T>(this, m_storage.size()-1, &m_storage.back().obj);
    }

    void newReference(int id)
    {
        m_storage[id].count++;
    }
    void deletedReference(int id)
    {
        m_storage[id].count--;
    }



private:
};



#endif // GENERICMANAGER_H
