#ifndef MEMORYFILE_H
#define MEMORYFILE_H

#include <vector>

/* representa um arquivo mapeado na memoria */
class MemoryFile
{
    public:
        MemoryFile();
        ~MemoryFile();

        void setData(std::vector<char>& other)
        {
            m_data = std::move(other);
            m_current_pos = 0;
        }

        int read(char* out, int n);

        void setPos(int p)
        {
            m_current_pos = p;
        }

        int getPos()
        {
            return m_current_pos;
        }
    private:
        unsigned int m_current_pos;
        std::vector<char> m_data;
};

#endif // MEMORYFILE_H
