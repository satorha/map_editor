#include "Resource.h"

#include "Logger/Logger.h"
#define LOG() Logger::Log("Resource")


Resource::Resource(const DirectoryTree* p)
{
    m_path = p;
//    m_recursive_ready = false;
    m_ready = false;
    m_error = false;
}

Resource::~Resource()
{

}

bool Resource::recursiveIsReady()
{
    //TODO: quando esta função retornar true alguma vez, todas as chamadas subsequentes retornarão true.
    //existe aqui uma chance de otimização
    if(!m_ready)
        return false;

    /*
        Passa por todas as dependencias.
        As que forem carregadas, saem da lista, pois não precisam ser checadas novamente
    */

    bool flag= (m_dependencies.size() > 0);
    auto it = m_dependencies.begin();
    while(it != m_dependencies.end())
    {
        if(!(*it)->recursiveIsReady())
        {
            return false;
        }
        it = m_dependencies.erase(it);
    }
    if(flag && (m_dependencies.size() == 0))
    {
        dependenciesReady();
    }

    return true;
}

std::string Resource::getFullPath()
{
    if(m_path == nullptr)
        return "";

    return m_path->getFullPath();
}

std::string Resource::getName()
{
     if(m_path == nullptr)
        return m_second_name;
    return m_path->getName();
}
