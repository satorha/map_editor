#include "TypeManager.h"
#include "Resource.h"
#include "AsyncResourceManager.h"
#include <set>
#include "Logger/Logger.h"
#define LOG() Logger::Log("TypeManager")


#define MAX_MANAGERS 10
#include "Util/Panic.h"

TypeManager* _registered_managers[MAX_MANAGERS] = {nullptr};


void addManager(TypeManager* m)
{
    for(int i=0; i < MAX_MANAGERS; i++)
        if(_registered_managers[i] == nullptr)
        {
            _registered_managers[i] = m;
            return;
        }

    LOG() << "Slots acabaram.";
    Panic("");
}

void removeManager(TypeManager* m)
{
    for(int i=0; i < MAX_MANAGERS; i++)
    {
        if(_registered_managers[i] == m)
        {
            _registered_managers[i] = nullptr;
            return;
        }
    }

    LOG() << "removeManager(): item não encontrado.";
}

void managers_logic()
{
    for(int i=0; i < MAX_MANAGERS; i++)
    {
        if(_registered_managers[i] == nullptr)
            continue;

        _registered_managers[i]->flushQueue();
    }
}

void managers_free_some_memory()
{
    LOG() << "Liberando recursos não utilizados.";

    int total = 0;
    for(int i=0; i < MAX_MANAGERS; i++)
    {
        if(_registered_managers[i] == nullptr)
            continue;

        total += _registered_managers[i]->eraseUnused();
    }

    LOG() << total << " objetos apagados.";
}


TypeManager::TypeManager()
{
    m_pedidos = m_concluidos = 0;
    addManager(this);
}


TypeManager::~TypeManager()
{
    removeManager(this);
}

void TypeManager::addToQueue(std::shared_ptr<Resource> res)
{
    m_to_process.push(res);
}

void TypeManager::flushQueue()
{
    std::shared_ptr<Resource> atual;

    while(m_to_process.nonblockingPop(atual))
    {
        atual->setError(!processItem(atual));
        atual->setReady();

        m_concluidos++;
        if(atual->getError())
        {
            Panic("Erro ao carregar recurso.");
        }
    }
}

std::shared_ptr<Resource> TypeManager::get(const char* str)
{
    auto ptr = find_ptr(str);

    if(ptr == nullptr) //objeto não existe ainda.
    {
        std::shared_ptr<Resource> novo = alloc(str);

        if(novo == nullptr)
        {
            LOG() << "Erro na alocação.";
            return nullptr;
        }
        store_ptr(novo);
        AsyncResourceManager::queue(novo, this); //bota na fila da thread de IO.
        m_pedidos++;
        return novo;
    }

    return ptr;
}

void TypeManager::AddDependecy(Resource* r, std::shared_ptr<Resource> dep)
{
    r->m_dependencies.push_back(dep);
}

