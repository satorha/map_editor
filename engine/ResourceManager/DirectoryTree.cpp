#include "DirectoryTree.h"
#include "Logger/Logger.h"
#include <stack>

#define LOG() Logger::Log("DirectoryTree")

DirectoryTree::DirectoryTree(const char* name, const DirectoryTree* parent)
{
    m_name = std::string(name);
    m_parent = parent;
}


DirectoryTree::DirectoryTree(DirectoryTree&& outro)
{
    m_parent = outro.m_parent;
    m_name = std::move(outro.m_name);
    m_current_file = std::move(outro.m_current_file);
    m_nodes = std::move(outro.m_nodes);
}

DirectoryTree::~DirectoryTree()
{

}

const DirectoryTree* DirectoryTree::getNode(const char* name) const
{
    auto it = m_nodes.find(name);
    if(it == m_nodes.end())
    {
        return nullptr;
    }
    return &it->second;
}

DirectoryTree* DirectoryTree::createNode(const char* name)
{
    if(m_current_file != nullptr)
    {
        LOG() << std::string("createNode(): Erro: um nodo arquivo não pode ter nodos filhos.");
        return nullptr;
    }

    const DirectoryTree* busca = getNode(name);
    if(busca == nullptr)
    {
        //ainda não existe um nodo com esse nome.
        auto retorno = m_nodes.insert(std::pair<std::string, DirectoryTree>(name, DirectoryTree(name, this)));
        return &retorno.first->second;
    }
    return (DirectoryTree*)busca;
}

void DirectoryTree::setFile(FileDescription* d)
{
    if(m_nodes.size() > 0)
    {
        LOG() << std::string("setFile(): Erro: Associar um arquivo a um nodo com filhos gera uma árvore inválida.");
        return;
    }
    d->setNode(this);
    m_current_file = std::unique_ptr<FileDescription>(d);
}

std::string DirectoryTree::getFullPath() const
{
    std::stack<std::string> nomes;

    std::string retorno;

    const DirectoryTree* atual = this;
    while(atual != nullptr)
    {
        std::string nome_atual = atual->getName();
        if(!nome_atual.empty())
        {
            if(!atual->isFile()) //se for um diretorio, adiciona um / no final
                nomes.push("/");
            nomes.push(nome_atual);
        }

        atual = atual->getParent();
    }

    while(!nomes.empty())
    {
        retorno += nomes.top();
        nomes.pop();
    }

    return retorno;
}


std::list<std::string> DirectoryTree::splitFilenameString(const std::string& filename)
{
    std::list<std::string> resultado;
    std::string buffer;
    for(size_t i=0; i < filename.size(); i++)
    {
        if(filename[i] == '/')
        {
            resultado.push_back(buffer);
            buffer.clear();
        }
        else
        {
            buffer.push_back(filename[i]);
        }
    }

    if(!buffer.empty())
        resultado.push_back(buffer);

    return resultado;
}


DirectoryTree* DirectoryTree::createFileRecursive(const char* path, FileDescription* file)
{
    std::list<std::string> caminho = DirectoryTree::splitFilenameString(path);

    DirectoryTree* atual = this;

    for(std::string& nome : caminho)
    {
        atual = atual->createNode(nome.c_str());
    }

    atual->setFile(file);
    return atual;
}

const DirectoryTree* DirectoryTree::findNode(const char* filename) const
{
    std::list<std::string> caminho = DirectoryTree::splitFilenameString(filename);

    const DirectoryTree* atual = this;

    for(std::string& nome : caminho)
    {
        atual = atual->getNode(nome.c_str());
        if(atual == nullptr)
        {
            return NULL;
        }
    }

    return atual;
}

void DirectoryTree::listFiles(std::vector<const DirectoryTree*>& out) const
{
    for(auto& entry : m_nodes)
    {
        const DirectoryTree& atual = entry.second;
        if(atual.isFile())
            out.push_back(&atual);
    }
}

void DirectoryTree::listFilesRecursive(std::vector<const DirectoryTree*>& out) const
{
    listFiles(out);
    for(auto& entry : m_nodes)
    {
        const DirectoryTree& atual = entry.second;
        if(!atual.isFile())
            atual.listFilesRecursive(out);
    }
}

