#ifndef DIRECTORYTREE_H
#define DIRECTORYTREE_H

#include <map>
#include <memory>
#include "FileDescription.h"
#include <list>
#include <vector>


class DirectoryTree
{
public:
    DirectoryTree(const char* name,const DirectoryTree* parent);
    DirectoryTree(DirectoryTree&& outro);
    ~DirectoryTree();

    /*
        Adiciona um novo filho ao nodo atual.
        Retorna um ponteiro ao nodo recém adicionado.

        Se um nodo com este nome já existir, retorna o endereço ao nodo já existente.

        Se o nodo atual estiver definido como um arquivo, ele não poderá ter nodos filhos,
        portanto ocorrerá um erro.
    */
    DirectoryTree* createNode(const char* name);


    /*
        Busca um nodo entre os filhos do atual.
        Se não houver nenhum com o nome especificado, retorna nullptr

        Caso ache, retorna ponteiro ao nodo encontrado.

    */
    const DirectoryTree* getNode(const char* name) const;

    /*
        Define o nodo atual como um arquivo.

        Se o nodo atual já tiver filhos, ele não pode ser definido como um arquivo, portanto ocorrerá um erro.

        Após esta função, o arquivo irá pertencer a DirectoryTree. Ele será destruido assim q DirectoryTree for.
    */
    void setFile(FileDescription* d);


    /*
        Retorna o caminho inteiro do arquivo como uma string.
        Ex: pasta1\pasta2\arquivo.ext

    */
    std::string getFullPath() const;

    const std::string& getName() const
    {
        return m_name;
    }

    const DirectoryTree* getParent() const
    {
        return m_parent;
    }

    bool isFile() const
    {
        return (m_current_file != nullptr);
    }

    /*
        Pega o arquivo associado com o DirectoryTree atual.
        Se o nodo atual não for um arquivo, retorna null
    */
    FileDescription* getFile() const
    {
        return m_current_file.get();
    }

    /*
        Separa uma string representando um caminho completo em cada um de seus componentes.

        Ex: pasta1/pasta2/arquivo.bin

        geraria a lista:
        pasta1, pasta2, arquivo.bin
    */
    static std::list<std::string> splitFilenameString(const std::string& filename);


    /*
        Partindo do nodo atual(this), cria todos os diretórios intermediários até chegar no arquivo.


        Ex: criar pasta/pasta2/arquivo.c
        Este método iria criar os diretórios pasta e pasta2 caso eles não existissem.
    */
    DirectoryTree* createFileRecursive(const char* path, FileDescription* file);

    /*
        Dado um caminho relativo ao nodo atual, retorna o ponteiro para o nodo especificado.
        Caso o nodo não exista, retorna nullptr
    */
    const DirectoryTree* findNode(const char* filename) const;

    /*
        Retorna um ponteiro pra cada filho do nodo atual que representa um arquivo
    */
    void listFiles(std::vector<const DirectoryTree*>& out) const;

    /*
        Retorna um ponteiro pra todos os nodos abaixo do atual que representarem um arquivo.
    */
    void listFilesRecursive(std::vector<const DirectoryTree*>& out) const;
private:
    const DirectoryTree* m_parent;
    std::string m_name;
    std::unique_ptr<FileDescription> m_current_file;

    std::map<std::string, DirectoryTree> m_nodes;
};

#endif // DIRECTORYTREE_H
