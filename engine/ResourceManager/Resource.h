#ifndef RESOURCE_H
#define RESOURCE_H

#include <string>
#include <cstdint>
#include <list>
#include <atomic>
#include <memory>
#include <iostream>

#include "DirectoryTree.h"
#include "TypeManager.h"

/*
    Essa classe pode ser usada como referencia generica
*/
class Resource  : public std::enable_shared_from_this<Resource>
{
public:
    explicit Resource(const DirectoryTree* path);

    Resource & operator=(const Resource&) = delete;
    Resource(const Resource&) = delete;

    virtual ~Resource();

    /*
        Retorna true quando a flag ready estiver true e ao mesmo tempo
        todos os objetos definidos como dependencia também estiverem com a flag ready.

        Se pelo menos um dos objetos na árvore não estiver pronto, retorna false.
    */
    bool recursiveIsReady();

    bool isReady()
    {
        return m_ready;
    }

    bool getError()
    {
        return m_error;
    }

    std::string getFullPath();

    std::string getName();
    //std::string getFilename();

    void setReady()
    {
        m_ready = true;
    }

    void setSecondName(std::string n)
    {
        m_second_name = n;
    }

    /*
         É chamada depois por recursiveIsReady() quando as dependencias utilizadas por este recurso ficarem prontas.
         Ou seja, a primeira vez q recursiceIsReady() retornar true
    */
    virtual void dependenciesReady(){};
protected:
    //friend class AsyncResourceManager;
    friend void io_thread_main();
    /*
        Essa função vai ser chamada na thread de IO, logo após os dados do arquivos terminarem de ser lidos.
        Nesta função deve-se implementar a decodificação destes dados.

        O ponteiro self aponta para o proprio objeto. É uma versão shared_ptr de 'this'

        m_error deve ser definida como true caso ocorra algum erro.
    */
    virtual void onDataAvailable(std::vector<char>& data) =0;


    void addDependency(std::shared_ptr<Resource> coisa)
    {
        if(coisa == nullptr)
            return;
        m_dependencies.push_back(coisa);
    }




private:
    friend class TypeManager;


    /*
        Se este resource depender de outras referências, elas devem ser registradas aqui.
        Assim, quando a função recursiveReady() for chamada, ela retornará true se toda a
        árvore de dependencias deste objeto estiver pronta pra uso.

        Este shared_ptr só é mantido enquanto o recurso que ele estiver não estiver pronto.
        Assim que este ficar com a flag ready, será removido do vetor na próxima chamada recursiveIsReady()

        Vale lembrar que adicionar uma dependencia com addDependecy não garante posse do objeto.
        Para evitar que ele seja deletado, uma shared_ptr deve ser associada a cada campo.
    */
    std::list<std::shared_ptr<Resource> > m_dependencies;

    std::atomic<bool> m_ready;

    std::string m_second_name;


    /*
        em caso de algum erro no carregamento, m_error será false.
        Neste caso os dados serão inválidos e portanto não devem ser utilizados.
    */
    bool m_error;

    //caminho que o recurso foi carregado
    const DirectoryTree* m_path;

    int m_type;

     void setError(bool v)
    {
        m_error = v;
    }




};

/*
    TESTE DE COMO O GERENCIADOR DE TIPO ESPECIFICO VAI FUNCIONAR

*/
/*
class ExampleManager: public TypeManager
{
public:
    bool processItem(std::shared_ptr<Resource> res)
    {
        res->setReady();
        std::cout << res->getFilename() << " esta pronto.\n";
        return true;
    }

    int eraseUnused()
    {
        return 0;
    }

protected:
    virtual std::shared_ptr<Resource> alloc(const char* filename)
    {
        return std::shared_ptr<Resource>(new Resource(filename));
    }

    void store_ptr(std::shared_ptr<Resource> ptr)
    {
        carregados[ptr->getFilename()] = ptr;
    }
    std::shared_ptr<Resource> find_ptr(const char* filename)
    {
        auto it = carregados.find(filename);
        if(it == carregados.end())
            return nullptr;

        return it->second;
    }

private:
    std::map<std::string, std::shared_ptr<Resource> > carregados;


};*/


#endif // RESOURCE_H
