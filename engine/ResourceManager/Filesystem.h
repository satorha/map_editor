#ifndef FILESYSTEM_H
#define FILESYSTEM_H
#include <fstream>
#include <string>
#include <map>
#include <memory>
#include <vector>
#include "FileDescription.h"
#include "DirectoryTree.h"
#include "MemoryFile.h"


class Filesystem
{
public:
    /*
        Inicia o gerenciador de arquivos.
        Caso já tenha sido iniciado, faz nada.
    */
    static void init();


    /*
        A engine mantém um sistema de arquivos virtual.
        Esta função pode ser usada para expor diretórios reais neste sistema de arquivos virtual.

        o conteúdo do diretório será visível em root. portanto, se o usuário adicionar o diretório pasta1\,
        então todos os conteúdos da pasta1\ estarão visivéis no diretório raiz do sistema de arquivos virtual.

    */
    static bool addDiskPath(const char* path);


    /*
        Faz algo semelhante a função acima porém, em vez de pegar os arquivos de algum diretório, pega arquivos
        contidos em um container no formato próprio da engine.
    */
    static bool addResourceFile(const char* path); //TODO

    /*
        Armazena um certo arquivo no vetor out.
        Em caso de erro (como o arquivo não existir) retorna false.
    */
    static bool readFile(const char* filename, std::vector<char>& out);

    /*
        semelhante a função acima, mas gera um objeto que permite uma leitura mais elegante dos dados.

        Em caso de erro, retorna um ponteiro nulo (nullptr)
    */
    static std::unique_ptr<MemoryFile> getMemoryMappedFile(const char* filename);

    /*
        Retorna o tamanho do arquivo.
        -1 em caso de erro (arquivo não encontrado?)
    */
    static int getFileSize(const char* filename);


    /*
        Salva todos os arquivos atualmente no índice para um único arquivo de recursos.
        retorna false em caso de erro.
    */
    static bool createResourceFile(const char* filename); //TODO


    /*
        Retorna uma lista com o caminho/nome de todos os arquivos atualmente no sistema de arquivos virtual.
    */
    static void getFilenames(std::vector<std::string>& saida);


    /*
        Diz se um certo caminho se trata de um arquivo ou diretório.
        Retorna true caso seja um arquivo.
    */
    static bool isFile(const char* path);


    static const DirectoryTree* getNode(const char* path);


private:

    Filesystem();

};

#endif // FILESYSTEM_H
