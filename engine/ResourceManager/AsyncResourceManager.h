#ifndef ASYNCRESOURCEMANAGER_H
#define ASYNCRESOURCEMANAGER_H

#include "Resource.h"
#include <memory>
#include "TypeManager.h"


class AsyncResourceManager
{

public:
    static void init();

    static void queue(std::shared_ptr<Resource> obj, TypeManager* t);

private:
    AsyncResourceManager();


};

void start_io_thread();
void end_io_thread();
#endif // ASYNCRESOURCEMANAGER_H
