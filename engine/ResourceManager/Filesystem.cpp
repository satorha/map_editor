#include "Filesystem.h"
#include "Logger/Logger.h"
#include "DirectoryTree.h"



/*
    Função de conveniência que será usada pra listar todos os arquivos e dentro de um caminho especificado.
    As subpastas também são incluídas na busca.
*/
#include <boost/filesystem.hpp>

bool ListarArquivosDaPasta(const char* folder, std::vector<std::string>& saida)
{
    boost::filesystem::path caminho(folder);

    if(!is_directory(caminho))
    {
        return false;
    }

    saida.clear();

    for ( boost::filesystem::recursive_directory_iterator end, dir(folder); dir != end; ++dir)
    {
        if(!is_directory(dir->path()))
            saida.push_back(dir->path().string().c_str()+2);
    }

    return true;
}


/*
    Aqui começa a implementação da interface dos arquivos
*/

DirectoryTree _root_dir("", nullptr);

DirectoryTree* add_file_recursively(const char* filename, FileDescription* f)
{
    return _root_dir.createFileRecursive(filename, f);

}

FileDescription* find_file(const char* str)
{
    return _root_dir.findNode(str)->getFile();
}
void Filesystem::init()
{

}


bool Filesystem::addDiskPath(const char* path)
{
    std::vector<std::string> arquivos;

    if(!ListarArquivosDaPasta(path, arquivos))
        return false;

    for(std::string& arquivo_atual : arquivos)
    {
        add_file_recursively(arquivo_atual.c_str(), new DiskFile(arquivo_atual.c_str()));

    }


    return true;

}


bool Filesystem::readFile(const char* filename, std::vector<char>& out)
{
    FileDescription* arquivo_atual = find_file(filename);
    if(arquivo_atual == nullptr)
        return false;

    out.resize(arquivo_atual->getSize()); //Espero que a alocação não falhe
    arquivo_atual->read(&out[0], out.size());

    return true;
}

int Filesystem::getFileSize(const char* filename)
{
    FileDescription* arquivo_atual = find_file(filename);

    if(arquivo_atual == nullptr)
        return -1;

    return arquivo_atual->getSize();
}

void Filesystem::getFilenames(std::vector<std::string>& saida)
{
    std::vector<const DirectoryTree*> files;
    files.reserve(128);

    _root_dir.listFilesRecursive(files);

    for(const DirectoryTree* f : files)
        saida.push_back(f->getFullPath());
}

bool Filesystem::isFile(const char* path)
{
    FileDescription* arquivo_atual = find_file(path);
    return arquivo_atual != nullptr;
}

std::unique_ptr<MemoryFile> Filesystem::getMemoryMappedFile(const char* filename)
{
    std::vector<char> dados;
    if(!readFile(filename, dados))
        return nullptr;

    std::unique_ptr<MemoryFile> arquivo(new MemoryFile);
    arquivo->setData(dados);

    return arquivo;
}

const DirectoryTree* Filesystem::getNode(const char* path)
{
    return _root_dir.findNode(path);
}
