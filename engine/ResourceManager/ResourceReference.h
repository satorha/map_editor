#ifndef RESOURCEREFERENCE_H
#define RESOURCEREFERENCE_H
#include <algorithm>

template<typename T> class GenericManager;

template<typename T>
class ResourceRef
{
public:
    explicit ResourceRef(GenericManager<T>* m=nullptr, int id=-1, T* ptr=nullptr)
    {
        m_id = id;
        m_parent = m;
        m->newReference(id);
        m_ptr = ptr;
    }

    ResourceRef(const ResourceRef& outro)
    {
        m_id = outro.m_id;
        m_parent = outro.m_parent;
        m_parent->newReference(m_id);
        m_ptr = outro.m_ptr;
    }

    friend void swap(ResourceRef& first, ResourceRef& second)
    {
        std::swap(first.m_id, second.m_id);
        std::swap(first.m_parent, second.m_parent);
        std::swap(first.m_ptr, second.m_ptr);
    }
    ResourceRef& operator=(ResourceRef outro)
    {
        swap(*this, outro);
        return *this;
    }
    ~ResourceRef()
    {
        m_parent->deletedReference(m_id);
    }


private:
    int m_id;
    GenericManager<T>* m_parent;
    T* m_ptr;


};

#endif // RESOURCEREFERENCE_H
