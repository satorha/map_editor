#ifndef FILEDESCRIPTION_H
#define FILEDESCRIPTION_H

#include <string>

class DirectoryTree;

/*
    Define a interface que descreve um arquivo na engine.
    Pode-se derivar desta classe para extender a engine e suportar arquivos de outro tipo.
    Por exemplo, um arquivo em disco ou em um arquivo de resource.
*/
class FileDescription
{
private:
    DirectoryTree* m_node; //ponteiro ao nodo que representa este arquivo
public:
    FileDescription();
    virtual int getSize()=0;

    /*
        Ler até n caracteres do arquivo no vetor out.
        Retorna a quantidade de bytes lidos.
    */
    virtual int read(char* out, int n) const=0;
    virtual std::string getName()=0;

    std::string getExtension();

    DirectoryTree* getNode();
    void setNode(DirectoryTree* t);
};


/*
    Referencia a um arquivo em disco
*/
class DiskFile: public FileDescription
{
public:
    DiskFile(const char* c_str);

    int getSize();
    int read(char* out, int n) const;
    std::string getName();

private:
    std::string m_name;
};
#endif // FILEDESCRIPTION_H
