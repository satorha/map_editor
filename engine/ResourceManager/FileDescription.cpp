#include "FileDescription.h"

#include <fstream>
std::ifstream::pos_type filesize(const char* filename)
{
    std::ifstream in(filename, std::ifstream::ate | std::ifstream::binary);
    if(!in.is_open()) return 0;
    return in.tellg();
}

FileDescription::FileDescription()
{
    m_node = nullptr;
}

DirectoryTree* FileDescription::getNode()
{
    return m_node;
}
void FileDescription::setNode(DirectoryTree* t)
{
    m_node = t;
}

std::string FileDescription::getExtension()
{
    std::string filename = getName();
    return filename.substr(filename.find_last_of('.'));
}


/*
    Implementação da classe que dá acesso direto a arquivos no disco.

*/
DiskFile::DiskFile(const char* str) :
    m_name(str)
{

}

int DiskFile::getSize()
{
    return filesize(m_name.c_str());
}

int DiskFile::read(char* out, int n) const
{
    std::fstream arquivo(m_name.c_str(), std::fstream::in | std::fstream::binary);

    if(!arquivo.is_open())
        return 0;

    arquivo.read(out, n);

    return arquivo.gcount();
}

std::string DiskFile::getName()
{
    return m_name;
}
