#include "AsyncResourceManager.h"

#include "MultithreadDataStructure/MultithreadQueue.h"

#include <thread>
#include "Filesystem.h"
#include <vector>

#include "Logger/Logger.h"

#define LOG() Logger::Log("AsyncResourceManager")
#include <vector>

#include "TypeManager.h"

#include "Util/Panic.h"

MultithreadQueue< std::pair<std::shared_ptr<Resource>, TypeManager*> > _io_queue;

void AsyncResourceManager::queue(std::shared_ptr<Resource> obj2, TypeManager* t)
{
    _io_queue.push(std::pair<std::shared_ptr<Resource>, TypeManager*>(obj2, t));
}


#include "Renderer/Renderer.h"
void io_thread_main()
{
    //TODO: implementar forma de sair da thread
    std::vector<char> buffer;
    while(_io_queue.running())
    {
        std::pair<std::shared_ptr<Resource>, TypeManager*>  atual = _io_queue.pop();


        std::shared_ptr<Resource>& res = atual.first;


        if(res == nullptr) break;

        /*
            Se ocorrer um erro na leitura do arquivo, o tamanho do buffer será zero.
            O TypeManager deverá detectar isso e emitir um erro.
        */
        if(!Filesystem::readFile(res->getFullPath().c_str(), buffer))
        {
            LOG() << "Erro ao ler arquivo \"" << res->getFullPath() << "\"";
            Panic("Falha na leitura de um recurso.");
            continue;
        }

        std::cout << "lendo " << res->getFullPath() << std::endl;
        LOG() << "Leu arquivo \"" << res->getFullPath() <<  "\" (" << buffer.size() << " bytes).";

        res->onDataAvailable(buffer);
        atual.second->addToQueue(res);

    }
}
std::thread first;

void start_io_thread()
{
    first = std::thread(io_thread_main);
}

void end_io_thread()
{
    _io_queue.requestQuit();
    first.join();
}
void AsyncResourceManager::init()
{
    start_io_thread();
}
