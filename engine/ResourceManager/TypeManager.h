#ifndef TYPEMANAGER_H
#define TYPEMANAGER_H

#include <memory>
#include <iostream>
#include <map>
#include "MultithreadDataStructure/MultithreadQueue.h"
class Resource;


class TypeManager
{
public:
    TypeManager();
    virtual ~TypeManager();


    /*
        Adiciona o objeto pra fila do pos-processamento.
        Depois desta ultima etapa, o objeto vai finalmente estar pronto pra uso.

        Esta função é usada pela AsyncResourceManager e nunca deve ser chamada diretamente.

    */
    void addToQueue(std::shared_ptr<Resource> res);


    /*
        Processa a fila de pos-processamento
        Esta função só pode ser chamada pela thread principal.
    */
    void flushQueue();

    /*
        Essa funcao define oq fazer no final.
        Ao final desta função, a flag ready do objeto será true.

        Em caso de erro, a função deve retornar false.
        É ideal que uma mensagem de erro seja impressa no console.
        Quando esta função retorna false, a flag erro do objeto é setada.

        É nesta função que devem ser alocadas as dependencias do recurso atual. (Ex.: os sprites carregam as texturas)
    */
    virtual bool processItem(std::shared_ptr<Resource> res)=0;


    /*
        Pega um ponteiro pra um recurso.
        O recurso será carregado na localização retornada.

        Esta função também vai adicionar todas as dependencias do recurso em questão na fila de carregamento.

        Esta função não é threadsafe.
    */
    std::shared_ptr<Resource> get(const char* name);

    /*
        Apaga todos objetos que nao estao sendo utilizados
    */
    virtual int eraseUnused()=0;



    int pendingObjects()
    {
        return m_pedidos - m_concluidos;
    }
    int completedObjects()
    {
        return m_concluidos;
    }

protected:
    /*
        Aloca um resource com o filename especificado.
        Esta função não faz nenhum outro processamento além de um simples new
    */
    virtual std::shared_ptr<Resource> alloc(const char* filename)=0;


    /*
        Esta funcao serve pra pedir pra estrutura guardar um resource que vai ser carregado.
    */
    virtual void store_ptr(std::shared_ptr<Resource> res)=0;

    /*
        Acha um dos recursos salvos pela store_ptr()
    */
    virtual std::shared_ptr<Resource> find_ptr(const char* filename)=0;


    /*
        Define que o objeto r depende de dep.

        Isto impedirá que R seja deletado antes do objeto atual ser.

        Para testar se r e suas dependencias estão prontos pra uso, usar a Resource::recursiveIsReady()
    */
    void AddDependecy(Resource* r, std::shared_ptr<Resource> dep);
private:

    /*
        Fila dos próximos itens a serem processados
    */
    MultithreadQueue< std::shared_ptr<Resource> > m_to_process;

    int m_pedidos;
    int m_concluidos;


};

void managers_logic();
void managers_free_some_memory();

#endif // TYPEMANAGER_H
