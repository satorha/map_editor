#include "MemoryFile.h"

MemoryFile::MemoryFile()
{
    //ctor
    m_current_pos = 0;
}

MemoryFile::~MemoryFile()
{
    //dtor
}

int MemoryFile::read(char* out, int n)
{
    int pos_atual = 0;
    while(m_current_pos < m_data.size())
    {
        if(pos_atual >= n) break;
        out[pos_atual++] = m_data[m_current_pos++];
    }

    return pos_atual;
}


