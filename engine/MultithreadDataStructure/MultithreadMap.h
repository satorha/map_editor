#ifndef MULTITHREADMAP_H
#define MULTITHREADMAP_H

#include <map>
#include <string>
#include <mutex>
#include <condition_variable>

template <typename T2, typename T>
class MultithreadMap
{
public:

    bool get(const T2& key, T& out)
    {
        std::unique_lock<std::mutex> mlock(mutex_);
        auto it = map_.find(key);
        if(it == map_.end()) return false;
        out = std::move(it->second);
        return true;
    }


    void insert(T2 key, T& item)
    {
        std::unique_lock<std::mutex> mlock(mutex_);

        //TODO: notificar a inserção de elemento duplicado
        map_[key] = std::move(item);

        mlock.unlock();
        cond_.notify_one();
    }

    /*
        Chama alguma função para realizar alguma operação no _map
    */
    void op(void (*func)(std::map<T2, T>& m))
    {
        std::unique_lock<std::mutex> mlock(mutex_);
        func(map_);
    }


private:
    std::map<T2, T> map_;
    std::mutex mutex_;
    std::condition_variable cond_;
};

#endif // MULTITHREADMAP_H
