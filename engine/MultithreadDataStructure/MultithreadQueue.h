#ifndef MULTITHREADQUEUE_H
#define MULTITHREADQUEUE_H

#include <queue>
#include <mutex>
#include <condition_variable>

template <typename T>
class MultithreadQueue
{
public:
    MultithreadQueue()
    {
        quit = false;
    }

    T pop()
    {
        std::unique_lock<std::mutex> mlock(mutex_);
        while (queue_.empty())
        {
            if(quit)
            {
                //antes de sair, limpa a fila
                while(!queue_.empty())
                    queue_.pop();
                return T();
            }
            cond_.wait(mlock);
        }
        auto& item = queue_.front();
        queue_.pop();
        return item;
    }

    bool nonblockingPop(T& out)
    {
        std::unique_lock<std::mutex> mlock(mutex_);
        if (queue_.empty())
        {
            return false;
        }
        out = queue_.front();
        queue_.pop();
        return true;
    }


    void push(const T& item)
    {
        std::unique_lock<std::mutex> mlock(mutex_);
        queue_.push(item);
        mlock.unlock();
        cond_.notify_one();
    }

    void requestQuit()
    {
        std::unique_lock<std::mutex> mlock(mutex_);
        quit = true;

        mlock.unlock();
        cond_.notify_one();
    }

    bool running()
    {
        return !quit;
    }


private:
    bool quit;
    std::queue<T> queue_;
    std::mutex mutex_;
    std::condition_variable cond_;
};
#endif // MULTITHREADQUEUE_H
