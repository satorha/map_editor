#ifndef ENGINEMAIN_H
#define ENGINEMAIN_H

#include "Scene/Scene.h"

Scene* get_current_scene();

Scene* alloc_scene(); // aloca uma cena

void change_scene(Scene* nova_cena);

void registrar_controladores();

void startup();

void main_loop_iteraction();

void quit_engine();

#endif // ENGINEMAIN_H
