#include "Input.h"
#include "Renderer/Renderer.h"
#include "Scene/Scene.h"

#ifdef USAR_SDL
#include <SDL/SDL.h>
#endif

Input* _current_input = NULL;
Input::Input()
{
    _current_input = this;
    mouse_x = mouse_y = 0;
    botoesmouse[0] = botoesmouse[1] = botoesmouse[2] = false;

}

Input::~Input()
{
    //dtor
}

#ifndef USAR_SDL

enum EVENT_T
{
    _EVENT_MOUSEMOVE,
    _EVENT_MOUSEBUTTONDOWN,
    _EVENT_MOUSEBUTTONUP
};

struct EVENT
{
    int mouse_x;
    int mouse_y;
    int mouse_button;

    int tipo;
};

std::vector<EVENT> _event_queue;


#endif

bool _keystate[256] = {false};
bool Input::init()
{
    _current_input = new Input;

#ifdef USAR_SDL
    if(SDL_WasInit(SDL_INIT_VIDEO) == 0)
    {
        if(SDL_Init(SDL_INIT_VIDEO) != 0) return false;
    }
#else
#warning Sem a SDL, o subsistema de input é inútil
#endif
    _current_input->quitar = false;
    return true;
}

bool Input::getKeyState(int key) const
{
    return _keystate[key];
}

void Input::getMousePos(int& x, int& y) const
{
    x = mouse_x;
    y = mouse_y;
    //rotate_point(x, y, _video->getCameraRot()); //rotação desativada
}

void Input::processEvents(Scene* current_scene)
{
#ifdef USAR_SDL
    SDL_Event event;

    int mouse_b=0;

    while(SDL_PollEvent(&event))
    {
        if(current_scene == nullptr)
            continue;
        switch( event.type )
        {
        case SDL_MOUSEMOTION:          
            _mousemove(event.motion.x, event.motion.y);
            break;


        case SDL_MOUSEBUTTONDOWN:
           if(event.button.button == SDL_BUTTON_LEFT)
                {mouse_b = 0;}
            else if(event.button.button == SDL_BUTTON_RIGHT)
                {mouse_b = 1;}
            else
                {mouse_b = 2;}
           botoesmouse[mouse_b] = true;
           current_scene->notifyMouseButtonDown();
            break;


        case SDL_MOUSEBUTTONUP:
            if(event.button.button == SDL_BUTTON_LEFT)
                 {mouse_b = 0;}
             else if(event.button.button == SDL_BUTTON_RIGHT)
                 {mouse_b = 1;}
             else
                 {mouse_b = 2;}
            botoesmouse[mouse_b] = true;
            current_scene->notifyMouseButtonUp();
            break;

        case SDL_QUIT:
            quitar = true;
            break;

        case SDL_KEYDOWN:
            _keystate[event.key.keysym.sym] = true;
            current_scene->notifyKeydown(event.key.keysym.sym);
            break;

        case SDL_KEYUP:
            _keystate[event.key.keysym.sym] = false;
            current_scene->notifyKeyup(event.key.keysym.sym);
            break;

        }

    }
#else
    for(EVENT& atual : _event_queue)
    {
        switch(atual.tipo)
        {
        case _EVENT_MOUSEBUTTONDOWN:
            botoesmouse[atual.mouse_button] = true;
            current_scene->notifyMouseButtonDown();
            break;

        case _EVENT_MOUSEBUTTONUP:
            botoesmouse[atual.mouse_button] = false;
            current_scene->notifyMouseButtonUp();
            break;
        default:

            break;
        }
    }

    _event_queue.clear();
#endif
}
#include <QDebug>
void Input::_mousemove(int x, int y)
{
    float ponto_superior_x = Renderer::cameraRect().getX() - Renderer::cameraRect().getW()/2;
    float ponto_superior_y = Renderer::cameraRect().getY() - Renderer::cameraRect().getH()/2;
    float razao_mouse_w = Renderer::cameraRect().getW()/Renderer::getWindowWidth();
    float razao_mouse_h = Renderer::cameraRect().getH()/Renderer::getWindowHeight();

    mouse_x = (x*razao_mouse_w) + ponto_superior_x;
    mouse_y = (y*razao_mouse_h) + ponto_superior_y;
}

void Input::_mousebuttonup(int b)
{
    EVENT novo;
    novo.mouse_button = b;
    novo.tipo = _EVENT_MOUSEBUTTONUP;
    _event_queue.push_back(novo);
}

void Input::_mousebuttondown(int b)
{
    EVENT novo;
    novo.mouse_button = b;
    novo.tipo = _EVENT_MOUSEBUTTONDOWN;
    _event_queue.push_back(novo);
}


bool Input::mustQuit()
{
    return quitar;
}

void Input::quit()
{
    delete _current_input;
}
