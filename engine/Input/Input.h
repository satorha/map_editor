#ifndef INPUT_H
#define INPUT_H

#include <vector>
#include "keys.h"

class Scene;

enum input_event_type
{
    INPUT_KEYUP,
    INPUT_KEYDOWN,
    INPUT_QUIT,
    INPUT_MOUSEBUTTONDOWN,
    INPUT_MOUSEBUTTONUP,
};

struct input_event
{
    int type;
    int key;
};
class Input
{
public:
    ~Input();

    static bool init();

    bool getKeyState(int tecla) const;
    void getMousePos(int& x, int& y) const;

    bool mustQuit();

    static void quit();
    bool mouseButtonState(int id)
    {
        return botoesmouse[id];
    }

    void processEvents(Scene* current_scene);


    void _mousemove(int x, int y);
    void _mousebuttonup(int b);
    void _mousebuttondown(int b);


private:

    Input();


    bool quitar;
    int mouse_x;
    int mouse_y;

    int centro_y;
    int centro_x;
    bool botoesmouse[3];
};

extern Input* _current_input;
#endif // INPUT_H
