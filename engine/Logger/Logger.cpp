#include "Logger.h"
#include <iostream>
#include <fstream>

std::fstream arquivo_log;

std::ostream& Logger::Log(const char* sender)
{
    if(!arquivo_log.is_open())
    {
        arquivo_log.open("log.txt", std::fstream::out);
    }
    arquivo_log << std::endl << "[" << sender << "] ";
    return arquivo_log;
}

