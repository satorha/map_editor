#ifndef LOGGER_H
#define LOGGER_H

#include <string>
#include <initializer_list>
#include <iostream>

const char _log_filename[] = "engine_log.txt";


class Logger
{
public:


    static std::ostream& Log(const char* sender);

protected:

private:


};



#endif // LOGGER_H
