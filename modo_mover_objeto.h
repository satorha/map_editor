#ifndef MODO_MOVER_OBJETO_H
#define MODO_MOVER_OBJETO_H

#include "engine/Scene/Entity.h"
#include "controlador_janela_base.h"
class modo_mover_objeto : public controlador_janela_base
{
public:
    explicit modo_mover_objeto(Scene* cena);

    void onMouseButtonDown(int x, int y);
    void onMouseButtonUp(int x, int y);
    void onFrame();
private:
    Entity* selected;
    Entity* arrastando;

    int vetor_x;
    int vetor_y;

};

#endif // MODO_MOVER_OBJETO_H
