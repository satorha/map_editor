#include "ponto_redimensionador.h"

ponto_redimensionador::ponto_redimensionador()
{
    m_parent = nullptr;
    m_pos = -1;
}

const char meu_nome[] = "ponto_redimensionador";
const char* ponto_redimensionador::getName()
{
    return meu_nome;
}

void ponto_redimensionador::eventMouseButtonDown()
{
    m_parent->ponto_redimensionador_pressionado(m_pos);
}


Interface* alocar_ponto_redimensionador()
{
    return new ponto_redimensionador();
}

