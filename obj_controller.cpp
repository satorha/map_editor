#include "obj_controller.h"

#include "engine/Scene/Scene.h"
#include "engine/Util/CollisionTest.h"
#include "engine/Input/Input.h"
#include "ponto_redimensionador.h"

const char _nome[] = "obj_controller";
#include <QDebug>

const char* obj_controller::getName()
{
    return _nome;
}

obj_controller::obj_controller()
{
    arrastando = current_timer = -1;
    m_markers[0] = nullptr;
}

void obj_controller::eventMouseButtonDown()
{

}

void obj_controller::eventMouseButtonUp()
{

}

void obj_controller::spawn_markers()
{
    if(m_markers[0] != nullptr)
    {
        qDebug() << "os pontos pra redimensionamento já foram criados.";
        return;
    }
    getScene()->registerEvent(this, EVENT_ANYMOUSEUP);
    for(int i=0; i < 4; i++)
    {
        m_markers[i] = static_cast<Sprite*>(getScene()->createObject(nullptr, OBJ_SPRITE, "ponto_redimensionador"));
        ponto_redimensionador* atual = static_cast<ponto_redimensionador*>(m_markers[i]->getController());
        atual->setParent(this);
        atual->setPos(i);
        m_markers[i]->setLayer(1);
        m_markers[i]->setColor(0, 255, 255);
        getScene()->setObjSize(m_markers[i], 10, 10);
    }

    current_timer = getScene()->registerTimer(this, 0, false, 0);

    eventMoved();
}
void obj_controller::destroy_markers()
{
    if(m_markers[0] == nullptr)
        return;

    getScene()->deregisterEvent(this, EVENT_ANYMOUSEUP);

    for(int i=0; i < 4; i++)
    {
        getScene()->deleteObject(m_markers[i]);
        m_markers[i] = nullptr;
    }

    getScene()->deregisterTimer(current_timer);
    current_timer = -1;

}

void obj_controller::eventAnyMouseUp()
{
    arrastando = -1;
}

void obj_controller::eventDelete()
{
    destroy_markers();
}

void obj_controller::eventBegin()
{
   getEntity()->setLayer(0);
}

int vetores[] = {
  0, -1,
  1, 0,
  0, 1,
  -1, 0
};
void obj_controller::eventMoved()
{
    if(m_markers[0] == nullptr)
        return;

    for(int i=0;i < 4; i++)
    {
        getScene()->setObjPos(m_markers[i], getEntity()->getArea().getX() + getEntity()->getArea().getW()*vetores[2*i]/2,
                                            getEntity()->getArea().getY() + getEntity()->getArea().getH()*vetores[2*i + 1]/2);
    }
}

void obj_controller::eventResized()
{
    eventMoved();
}

void obj_controller::timerCallback(Timer* t)
{
    int mouse_x;
    int mouse_y;

    bool mouse_button;

    mouse_button = _current_input->mouseButtonState(0);
    _current_input->getMousePos(mouse_x, mouse_y);

    if(mouse_button)
        change_size(mouse_x, mouse_y);

}

void obj_controller::change_size(int x, int y)
{
    Sprite* atual = (Sprite*)getEntity();
    Scene* cena = (Scene*)getScene();

    float topo = atual->getArea().getY() - atual->getArea().getH()/2;
    float fundo = atual->getArea().getY() + atual->getArea().getH()/2;

    float esquerda = atual->getArea().getX() - atual->getArea().getW()/2;
    float direita = atual->getArea().getX() + atual->getArea().getW()/2;
    switch(arrastando)
    {
    case 0:
        topo = y;
        break;
    case 2:
        fundo = y;
        break;
    case 3:
        esquerda = x;
        break;
    case 1:
        direita = x;
        break;
    default:
        return;
        break;
    }


    getScene()->setObjSize(atual, direita - esquerda, fundo - topo);
    getScene()->setObjPos(atual, (esquerda+direita)/2, (topo + fundo)/2);

}

void obj_controller::ponto_redimensionador_pressionado(int i)
{
    arrastando = i;
}

Interface* alocar_obj_controller()
{
    return new obj_controller();
}
