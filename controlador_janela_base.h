#ifndef CONTROLADOR_JANELA_BASE_H
#define CONTROLADOR_JANELA_BASE_H

class Scene;
class Entity;
class controlador_janela_base
{
public:
    explicit controlador_janela_base(Scene* cena);

    virtual void onMouseButtonDown(int x, int y)=0;
    virtual void onMouseButtonUp(int x, int y)=0;
    virtual void onFrame()=0;

    Scene* getScene()
    {
        return current_scene;
    }
protected:
    void setSelected(Entity* coisa);

private:
    Scene* current_scene;
};

#endif // CONTROLADOR_JANELA_BASE_H
