#include "modo_mover_objeto.h"
#include "engine/Scene/Scene.h"
#include "obj_controller.h"

modo_mover_objeto::modo_mover_objeto(Scene* cena) :
    controlador_janela_base(cena)
{
    selected = nullptr;
    arrastando = nullptr;
}

#include <QDebug>

void modo_mover_objeto::onMouseButtonUp(int x, int y)
{
    arrastando = nullptr;
    if(selected != nullptr)
        ((obj_controller*)selected->getController())->destroy_markers();

    selected = nullptr;
    auto lista = getScene()->getIntersections(Circle(x, y));

    if(lista.size() > 0)
    {
        //SELECIONA SEMPRE O primeiro da lista
        //TODO: oq fazer se tiver mais de um objeto ocupando o ponto selecionado?
        obj_controller* atual = static_cast<obj_controller*>(lista[0]->getController());

        if(atual == nullptr)
        {
            qDebug() << "breakpoint!";
        }
        qDebug() << "addr: " << atual;


        atual->spawn_markers();
        selected = lista[0];
    }

    setSelected(selected);

}

void modo_mover_objeto::onMouseButtonDown(int x, int y)
{
    auto lista = getScene()->getIntersections(Circle(x, y));

    if(lista.size() > 0)
    {
        arrastando = lista[0];
        vetor_x = arrastando->getX() - x;
        vetor_y = arrastando->getY() - y;
    }
}
#include "engine/Input/Input.h"
void modo_mover_objeto::onFrame()
{
    if(arrastando != nullptr)
    {
        int x, y;
        _current_input->getMousePos(x, y);

        getScene()->setObjPos(arrastando, x + vetor_x, y + vetor_y);
    }

}
