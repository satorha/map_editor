#include "glwidget.h"

#include "EngineMain.h"
#include "Scene/Sprite.h"
#include "Renderer/Renderer.h"
#include "Renderer/Texture/TextureManager.h"
#include "Input/Input.h"
#include <QTimer>
#include "engine/Scene/Controller/ControllerManager.h"
#include "obj_controller.h"
#include "ponto_redimensionador.h"

#include "modo_mover_objeto.h"

GLWidget::GLWidget(QWidget *parent) :
    QGLWidget(parent)
{

}

GLWidget::~GLWidget()
{
    quit_engine();
}

Sprite* obj;

void GLWidget::setMode(controlador_janela_base *n)
{
    modo_atual = std::unique_ptr<controlador_janela_base>(n);
}

void GLWidget::initializeGL()
{
    startup();



    ControllerManager::registerController(alocar_obj_controller);
    ControllerManager::registerController(alocar_ponto_redimensionador);

    _ena_ativa = alloc_scene();
    change_scene(_ena_ativa);

    setMode(new modo_mover_objeto(_ena_ativa));

    /*
    obj = (Sprite*)_ena_ativa->createObject("sprite2", OBJ_SPRITE, nullptr);
    Sprite::frame novo_f;
    novo_f.tex = std::static_pointer_cast<Texture>(texManager.get("4307.png"));
    obj->addFrame(novo_f);

    _ena_ativa->setObjPos(obj, Renderer::cameraRect().getX(), Renderer::cameraRect().getY());*/

    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(update()));
    timer->start(15);

}

#include <QDebug>
void GLWidget::resizeGL(int w, int h)
{
    Renderer::resizeCamera(w, h);
    Renderer::window_resized(w, h);
    //_ena_ativa->setObjSize(obj, Renderer::cameraRect().getW(), Renderer::cameraRect().getH());
}

void GLWidget::paintGL()
{
    QPoint p = mapFromGlobal(QCursor::pos());

    _current_input->_mousemove(p.x(), p.y());

    if(modo_atual)
        modo_atual->onFrame();
    main_loop_iteraction();
}

#include <QDebug>
void GLWidget::mouseReleaseEvent(QMouseEvent * event)
{
    _current_input->_mousebuttonup(0);
    if(modo_atual)
    {
        int x, y;
        _current_input->getMousePos(x, y);
        modo_atual->onMouseButtonUp(x, y);
    }
}

void GLWidget::mousePressEvent(QMouseEvent * event)
{
    _current_input->_mousebuttondown(0);
    if(modo_atual)
    {
        int x, y;
        _current_input->getMousePos(x, y);
        modo_atual->onMouseButtonDown(x, y);
    }
}
